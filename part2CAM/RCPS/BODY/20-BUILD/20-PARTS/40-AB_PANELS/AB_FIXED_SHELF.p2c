(_RUNDEFAULTHELPERRCP "AB_DEFAULT" nil "p2chelper")
(_RUNDEFAULTHELPERRCP "FIXED_SHELF_DEFAULT" nil "p2chelper")
(_RUNDEFAULTHELPERRCP "PANELS_JUNCTION_STYLES_DEFAULT" nil "p2chelper")
(_RUNDEFAULTHELPERRCP "EDGES_DEFAULT" nil "p2chelper")
(_RUNDEFAULTHELPERRCP "GROOVES_DEFAULT" nil "p2chelper")
; Control for cutlist and dxf rotation
(if (_NOTNULL ENDING_UNIT_CONTROL)
	(progn
		(if (null FIXED_SHELF_PANEL_CUTLIST_ROTATION) (_FSET (_ 'FIXED_SHELF_PANEL_CUTLIST_ROTATION __MODULBODYMATROT)))
		(if (null FIXED_SHELF_PANEL_DXF_ROTATION) (_FSET (_ 'FIXED_SHELF_PANEL_DXF_ROTATION __MODULBODYMATROT)))
		; According to whether there is extra side or not, material of edgestrips which is hidden by extra side is determined
		(if (_NOTNULL AB_EXTRA_SIDE_AVAILABLE)
			(progn
				(_FSET (_ 'extraSideShare __AD_PANELTHICK))
				(_FSET (_ 'sideShare __AD_PANELTHICK))
				(_FSET (_ 'backSideTolerance (+ GROOVE_WID GROOVE_DISTANCE)))
			)
			(progn		
				(_FSET (_ 'extraSideShare 0))
				(_FSET (_ 'sideShare __AD_PANELTHICK))
				(_FSET (_ 'backSideTolerance __AD_PANELTHICK))
			)
		)
		; Number of shelves are increased 2 for top and bottom panel
		(_FSET (_ 'shareOfPanels (* __AD_PANELTHICK (+ __SHELFNO 2.0))))
		(_FSET (_ 'numberOfDivisions (+ __SHELFNO 1.0)))
		(_FSET (_ 'heightForEachDivision (/ (- __HEI shareOfPanels) numberOfDivisions)))

		(_FSET (_ 'currentShelfELEV (+ __AD_PANELTHICK heightForEachDivision)))
		(_FSET (_ 'shelfCounter 1))
		(repeat __SHELFNO
			(_FSET (_ 'paramBody (_& (_ "AB_FIXED_SHELF_" shelfCounter "_"))))
			
			(_FSET (_ 'tempWID (_& (_ paramBody "WID"))))
			(_FSET (_ 'tempHEI (_& (_ paramBody "HEI"))))
			(_FSET (_ 'tempSIDEL (_& (_ paramBody "SIDEL"))))
			(_FSET (_ 'tempFRONTL (_& (_ paramBody "FRONTL"))))
			(_FSET (_ 'tempCHAML (_& (_ paramBody "CHAML"))))
			
			(_FSET (_ 'tempROT (_& (_ paramBody "ROT"))))
			(_FSET (_ 'tempMAT (_& (_ paramBody "MAT"))))
			(_FSET (_ 'tempTHICKNESS (_& (_ paramBody "THICKNESS"))))
			(_FSET (_ 'tempPDATA (_& (_ paramBody "PDATA"))))
			
			(_FSET (_ 'tempLABEL (_& (_ paramBody "LABEL"))))
			(_FSET (_ 'tempTAG (_& (_ paramBody "TAG"))))
			(_FSET (_ 'tempEDGESTRIPS (_& (_ paramBody "EDGESTRIPS"))))
			; This parameter will be used in operations
			(_FSET (_ 'tempELEV (_& (_ paramBody "ELEV"))))
			
			(_FSET (_ (read tempWID) (_= "__WID - sideShare - extraSideShare")))
			(_FSET (_ (read tempHEI) (_= "__DEP - backSideTolerance - FIXED_SHELF_FRONT_OFFSET")))
			(_FSET (_ (read tempSIDEL) (_= "__SIDEL - backSideTolerance - FIXED_SHELF_FRONT_OFFSET")))
			(_FSET (_ (read tempFRONTL) (- __FRONTL sideShare)))
			(_FSET (_ (read tempCHAML) __CHAML))
			
			(_FSET (_ (read tempROT) (_ FIXED_SHELF_PANEL_MAIN_ROTATION FIXED_SHELF_PANEL_CUTLIST_ROTATION FIXED_SHELF_PANEL_DXF_ROTATION FIXED_SHELF_PANEL_MIRRORING_AXIS)))
			(_FSET (_ (read tempMAT) __MODULBODYMAT))
			(_FSET (_ (read tempTHICKNESS) __AD_PANELTHICK))
			
			(_FSET (_ (read tempLABEL) (XSTR FIXED_SHELF_PANEL_LABEL)))
			(_FSET (_ (read tempTAG) FIXED_SHELF_PANEL_TAG))
			(_FSET (_ (read tempELEV) currentShelfELEV))
			(cond 
				((equal __MODULDIRECTION "L")
					; Edgestrip material controls
					(if (null FIXED_SHELF_LEFT_EDGESTRIP_MAT) (_FSET (_ 'FIXED_SHELF_LEFT_EDGESTRIP_MAT (if (_NOTNULL AB_EXTRA_SIDE_AVAILABLE) nil SHELF_FRONTSIDE_EDGES_MAT))))
					(if (null FIXED_SHELF_TOP_EDGESTRIP_MAT) (_FSET (_ 'FIXED_SHELF_TOP_EDGESTRIP_MAT (if (null AB_EXTRA_SIDE_AVAILABLE) nil SHELF_HIDDEN_EDGES_MAT))))
					(if (null FIXED_SHELF_BOTTOM_EDGESTRIP_MAT) (_FSET (_ 'FIXED_SHELF_BOTTOM_EDGESTRIP_MAT SHELF_FRONTSIDE_EDGES_MAT)))
					(if (null FIXED_SHELF_SLOPING_EDGESTRIP_MAT) (_FSET (_ 'FIXED_SHELF_SLOPING_EDGESTRIP_MAT SHELF_FRONTSIDE_EDGES_MAT)))
					
					(_ADDPARTMAT2EDGESTRIPS (_ 'FIXED_SHELF_LEFT_EDGESTRIP_MAT 'FIXED_SHELF_RIGHT_EDGESTRIP_MAT 'FIXED_SHELF_TOP_EDGESTRIP_MAT 'FIXED_SHELF_BOTTOM_EDGESTRIP_MAT 'FIXED_SHELF_SLOPING_EDGESTRIP_MAT) __MODULBODYMAT ADD_PART_MATERIAL)
					; Width of edgestrips are taken from database
					(_FSET (_ 'LEFT_EDGESTRIP_WID (_GETEDGESTRIPWIDFROMDB FIXED_SHELF_LEFT_EDGESTRIP_MAT)))
					(_FSET (_ 'RIGHT_EDGESTRIP_WID (_GETEDGESTRIPWIDFROMDB FIXED_SHELF_RIGHT_EDGESTRIP_MAT)))
					(_FSET (_ 'TOP_EDGESTRIP_WID (_GETEDGESTRIPWIDFROMDB FIXED_SHELF_TOP_EDGESTRIP_MAT)))
					(_FSET (_ 'BOTTOM_EDGESTRIP_WID (_GETEDGESTRIPWIDFROMDB FIXED_SHELF_BOTTOM_EDGESTRIP_MAT)))
					(_FSET (_ 'SLOPING_EDGESTRIP_WID (_GETEDGESTRIPWIDFROMDB FIXED_SHELF_SLOPING_EDGESTRIP_MAT)))
					
					(_FSET (_ (read tempPDATA) (_ (_ 0.0 0.0 0.0) 0 
												  (_ 0.0 (_S2V tempFRONTL) 0.0) 0 
												  (_ (- (_S2V tempHEI) (_S2V tempSIDEL)) (- (_S2V tempWID) FIXED_SHELF_SIDES_TOTAL_OFFSET) 0.0) 0 
												  (_ (_S2V tempHEI) (- (_S2V tempWID) FIXED_SHELF_SIDES_TOTAL_OFFSET) 0.0) 0 
												  (_ (_S2V tempHEI) 0.0 0.0) 0 
												  (_ 0.0 0.0 0.0) 0)))
					
					(_FSET (_ (read tempEDGESTRIPS) (_ (_ FIXED_SHELF_BOTTOM_EDGESTRIP_MAT BOTTOM_EDGESTRIP_WID)
													   (_ FIXED_SHELF_SLOPING_EDGESTRIP_MAT SLOPING_EDGESTRIP_WID)
													   (_ FIXED_SHELF_LEFT_EDGESTRIP_MAT LEFT_EDGESTRIP_WID)
													   (_ FIXED_SHELF_TOP_EDGESTRIP_MAT TOP_EDGESTRIP_WID)
													   (_ FIXED_SHELF_RIGHT_EDGESTRIP_MAT RIGHT_EDGESTRIP_WID))))
				)
				((equal __MODULDIRECTION "R")
					; Edgestrip material controls
					(if (null FIXED_SHELF_RIGHT_EDGESTRIP_MAT) (_FSET (_ 'FIXED_SHELF_RIGHT_EDGESTRIP_MAT (if (_NOTNULL AB_EXTRA_SIDE_AVAILABLE) nil SHELF_FRONTSIDE_EDGES_MAT))))
					(if (null FIXED_SHELF_TOP_EDGESTRIP_MAT) (_FSET (_ 'FIXED_SHELF_TOP_EDGESTRIP_MAT (if (null AB_EXTRA_SIDE_AVAILABLE) nil SHELF_HIDDEN_EDGES_MAT))))
					(if (null FIXED_SHELF_BOTTOM_EDGESTRIP_MAT) (_FSET (_ 'FIXED_SHELF_BOTTOM_EDGESTRIP_MAT SHELF_FRONTSIDE_EDGES_MAT)))
					(if (null FIXED_SHELF_SLOPING_EDGESTRIP_MAT) (_FSET (_ 'FIXED_SHELF_SLOPING_EDGESTRIP_MAT SHELF_FRONTSIDE_EDGES_MAT)))
					
					(_ADDPARTMAT2EDGESTRIPS (_ 'FIXED_SHELF_LEFT_EDGESTRIP_MAT 'FIXED_SHELF_RIGHT_EDGESTRIP_MAT 'FIXED_SHELF_TOP_EDGESTRIP_MAT 'FIXED_SHELF_BOTTOM_EDGESTRIP_MAT 'FIXED_SHELF_SLOPING_EDGESTRIP_MAT) __MODULBODYMAT ADD_PART_MATERIAL)
					; Width of edgestrips are taken from database
					(_FSET (_ 'LEFT_EDGESTRIP_WID (_GETEDGESTRIPWIDFROMDB FIXED_SHELF_LEFT_EDGESTRIP_MAT)))
					(_FSET (_ 'RIGHT_EDGESTRIP_WID (_GETEDGESTRIPWIDFROMDB FIXED_SHELF_RIGHT_EDGESTRIP_MAT)))
					(_FSET (_ 'TOP_EDGESTRIP_WID (_GETEDGESTRIPWIDFROMDB FIXED_SHELF_TOP_EDGESTRIP_MAT)))
					(_FSET (_ 'BOTTOM_EDGESTRIP_WID (_GETEDGESTRIPWIDFROMDB FIXED_SHELF_BOTTOM_EDGESTRIP_MAT)))
					(_FSET (_ 'SLOPING_EDGESTRIP_WID (_GETEDGESTRIPWIDFROMDB FIXED_SHELF_SLOPING_EDGESTRIP_MAT)))
					
					(_FSET (_ (read tempPDATA) (_ (_ 0.0 (- (_S2V tempWID) (_S2V tempFRONTL) FIXED_SHELF_SIDES_TOTAL_OFFSET) 0.0) 0 
												  (_ 0.0 (- (_S2V tempWID) FIXED_SHELF_SIDES_TOTAL_OFFSET) 0.0) 0 
												  (_ (_S2V tempHEI) (- (_S2V tempWID) FIXED_SHELF_SIDES_TOTAL_OFFSET) 0.0) 0 
												  (_ (_S2V tempHEI) 0.0 0.0) 0 
												  (_ (- (_S2V tempHEI) (_S2V tempSIDEL)) 0.0 0.0) 0 
												  (_ 0.0 (- (_S2V tempWID) (_S2V tempFRONTL) FIXED_SHELF_SIDES_TOTAL_OFFSET) 0.0) 0)))
					
					(_FSET (_ (read tempEDGESTRIPS) (_ (_ FIXED_SHELF_BOTTOM_EDGESTRIP_MAT BOTTOM_EDGESTRIP_WID)
													   (_ FIXED_SHELF_LEFT_EDGESTRIP_MAT LEFT_EDGESTRIP_WID)
													   (_ FIXED_SHELF_TOP_EDGESTRIP_MAT TOP_EDGESTRIP_WID)
													   (_ FIXED_SHELF_RIGHT_EDGESTRIP_MAT RIGHT_EDGESTRIP_WID)
													   (_ FIXED_SHELF_SLOPING_EDGESTRIP_MAT SLOPING_EDGESTRIP_WID))))
				)
			)
			(_FSET (_ 'currentShelfCODE (_& (_ (XSTR "ENDING_UNIT") "_" (XSTR "FIXED_SHELF") "_" shelfCounter))))
			
			(_PANELMAIN currentShelfCODE (_ (_S2V tempPDATA) (_S2V tempROT) (_S2V tempMAT) (_S2V tempTHICKNESS)))
			
			(_CREATEEDGESTRIPS currentShelfCODE (_S2V tempEDGESTRIPS))
			
			(_PUTLABEL currentShelfCODE currentShelfCODE (_S2V tempLABEL))
			(_PUTTAG currentShelfCODE currentShelfCODE (_S2V tempTAG))
			
			(_FSET (_ 'currentShelfELEV (_= "currentShelfELEV + __AD_PANELTHICK + heightForEachDivision")))
			(_FSET (_ 'shelfCounter (+ 1 shelfCounter)))
		)
	)
)
(_NONOTCH)