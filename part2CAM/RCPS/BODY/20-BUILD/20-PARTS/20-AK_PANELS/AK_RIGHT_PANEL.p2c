; -------------------------------------------------- DEFAULT RECIPES ---------------------------------------------------------

(_RUNDEFAULTHELPERRCP "AK_DEFAULT" nil "p2chelper")
(_RUNDEFAULTHELPERRCP "RIGHT_PANEL_DEFAULT" nil "p2chelper")
(_RUNDEFAULTHELPERRCP "PANELS_JUNCTION_STYLES_DEFAULT" nil "p2chelper")
(_RUNDEFAULTHELPERRCP "EDGES_DEFAULT" nil "p2chelper")
(_RUNDEFAULTHELPERRCP "GOLA_DEFAULT" nil "p2chelper")

(if (null CORNER_UNIT_CONTROL)
	(progn
		; There is no need for notch operations
		(_NONOTCH)
	)
	(progn
; -------------------------------------------------- EDGESTRIPS ---------------------------------------------------------
		; Edgestrip material controls
		(if (null AK_RIGHT_VISIBLE_EDGESTRIP_MAT) (_FSET (_ 'AK_RIGHT_VISIBLE_EDGESTRIP_MAT FRONTSIDE_EDGES_MAT)))
		(if (null AK_RIGHT_HIDDEN_EDGESTRIP_MAT) (_FSET (_ 'AK_RIGHT_HIDDEN_EDGESTRIP_MAT WALLSIDE_EDGES_MAT)))
		;(if (null AK_RIGHT_TOP_EDGESTRIP_MAT) (_FSET (_ 'AK_RIGHT_TOP_EDGESTRIP_MAT UPCAST_EDGES_MAT)))
		(if (null AK_RIGHT_TOP_EDGESTRIP_MAT) (_FSET (_ 'AK_RIGHT_TOP_EDGESTRIP_MAT (if (equal TOPSIDE_STYLE "TOP_ON_SIDES") nil UPCAST_EDGES_MAT))))
		(if (null AK_RIGHT_BOTTOM_EDGESTRIP_MAT) (_FSET (_ 'AK_RIGHT_BOTTOM_EDGESTRIP_MAT (if (equal BOTTOMSIDE_STYLE "SIDES_ON_BOTTOM") nil SIDES_BOTTOM_EDGES_MAT))))
		
		(_ADDPARTMAT2EDGESTRIPS (_ 'AK_RIGHT_VISIBLE_EDGESTRIP_MAT 'AK_RIGHT_HIDDEN_EDGESTRIP_MAT 'AK_RIGHT_TOP_EDGESTRIP_MAT 'AK_RIGHT_BOTTOM_EDGESTRIP_MAT) __MODULBODYMAT ADD_PART_MATERIAL)
		; Width of edgestrips are taken from database
		(_FSET (_ 'VISIBLE_EDGESTRIP_WID (_GETEDGESTRIPWIDFROMDB AK_RIGHT_VISIBLE_EDGESTRIP_MAT)))
		(_FSET (_ 'HIDDEN_EDGESTRIP_WID (_GETEDGESTRIPWIDFROMDB AK_RIGHT_HIDDEN_EDGESTRIP_MAT)))
		(_FSET (_ 'TOP_EDGESTRIP_WID (_GETEDGESTRIPWIDFROMDB AK_RIGHT_TOP_EDGESTRIP_MAT)))
		(_FSET (_ 'BOTTOM_EDGESTRIP_WID (_GETEDGESTRIPWIDFROMDB AK_RIGHT_BOTTOM_EDGESTRIP_MAT)))
		
; -------------------------------------------------- PANEL ROTATION ---------------------------------------------------------	
		; Control for cutlist and dxf rotation
		(if (null AK_RIGHT_PANEL_CUTLIST_ROTATION) (_FSET (_ 'AK_RIGHT_PANEL_CUTLIST_ROTATION __MODULBODYMATROT)))
		(if (null AK_RIGHT_PANEL_DXF_ROTATION) (_FSET (_ 'AK_RIGHT_PANEL_DXF_ROTATION __MODULBODYMATROT)))
		
		(_FSET (_ 'AK_RIGHT_PANEL_ROT (_ AK_RIGHT_PANEL_MAIN_ROTATION AK_RIGHT_PANEL_CUTLIST_ROTATION AK_RIGHT_PANEL_DXF_ROTATION AK_RIGHT_PANEL_MIRRORING_AXIS)))
		
; -------------------------------------------------- PANEL DIMENSION DATA ---------------------------------------------------------
		; Global variables of corner unit right panel
		(_FSET (_ 'AK_RIGHT_PANEL_WID (+ __DEP AK_RIGHT_PANEL_FRONT_VARIANCE AK_RIGHT_PANEL_BACK_VARIANCE)))
		(_FSET (_ 'AK_RIGHT_PANEL_HEI (_= " __HEI - bottomSideStyleV1 - topSideStyleV1")))
		
		(_FSET (_ 'AK_RIGHT_PANEL_MAT __MODULBODYMAT))

		(_FSET (_ 'AK_RIGHT_PANEL_CODE (_& (_ (XSTR "CORNER_UNIT") "_" (XSTR "RIGHT_PANEL")))))
		
		;;;;;;;;;;; GOLA_PREFERENCES;;;;;;;;;;;;;;;;		
		(if	(and (not (null GOLA_PATTERN))   (or (equal 2 GOLA_SIDE_SELECTION)(equal 3 GOLA_SIDE_SELECTION)))
			(progn
				(setq gCavityShift (_ 0 (_= "0 - bottomSideStyleV1") 0));;if necessary, offsetting cavity. Domestic or Euro styles for example
				(setq nCavityShift (_ 0 (_= "0 - bottomSideStyleV1") 0))
				(setq PanelInformations (_GENERATEPDATAV2 AK_RIGHT_PANEL_WID 
											   AK_RIGHT_PANEL_HEI 
											   0 
											   gCavityShift
											   nCavityShift
											   GOLA_CAVITY_ORDER
											   nil ))
				(setq GolaPData (MoveVectorList (MirrorVectorListByY (nth 0 PanelInformations)) (_  AK_RIGHT_PANEL_WID 0 0 ) ))
				(setq GolaEdgeStrips (mapcar '(lambda (item) 
													(if (equal item 0) (setq item (_ " " 0)))
													(if (equal item 1) (setq item (_ AK_RIGHT_VISIBLE_EDGESTRIP_MAT VISIBLE_EDGESTRIP_WID)))
													(if (equal item 2) (setq item (_ AK_RIGHT_TOP_EDGESTRIP_MAT TOP_EDGESTRIP_WID)))
													(if (equal item 3) (setq item (_ AK_RIGHT_HIDDEN_EDGESTRIP_MAT HIDDEN_EDGESTRIP_WID)))
													(if (equal item 4) (setq item (_ AK_RIGHT_BOTTOM_EDGESTRIP_MAT BOTTOM_EDGESTRIP_WID)))
													item ) (nth 1 PanelInformations)))
				(setq golaControlPoints (mapcar '(lambda (item) (MoveVectorList (MirrorVectorListByY item)(_  AK_RIGHT_PANEL_WID 0 0 ) ) ) (nth 2 PanelInformations)) )
				(setq golaEdgeTrimCtrlPts (mapcar '(lambda (item) (MoveVectorList (MirrorVectorListByY (reverse item))(_  AK_RIGHT_PANEL_WID 0 0 ) ) ) (nth 4 PanelInformations)) )
				
				; milling approach lines adding 
				(if (or (equal GOLA_DRAW_APPROACH_OUTSIDE 1) (equal GOLA_DRAW_SECONDARY_FACE_APPROACH_OUTSIDE 1))
					(progn
						(_GENERATEMILLINGAPPROACH AK_RIGHT_PANEL_CODE 
												  golaControlPoints                                                      ; definition of up L , down L , U cavities control points 
												  GOLA_DRAW_APPROACH_OUTSIDE											 ; Draw top approach lines
												  GOLA_DRAW_SECONDARY_FACE_APPROACH_OUTSIDE      						 ; Draw bottom approach lies
												  GOLA_MIRROR_SFACE_GEOMETRIES											 ; Mirror bottom approach lines. it needed for biesse cam			
												  GOLA_APPROACH_DISTANCE 												 ; Desired approach distance from the outside of panels
												  GOLA_INNER_RADIUS														 ; Definition of the cavities concave corner radius
												  (_ (_ 0.0 (/ AK_RIGHT_PANEL_HEI 2) 0.0) (_ 1.0 (/ AK_RIGHT_PANEL_HEI 2) 0.0) )); Mirror axis
						(_GENERATEEDGEMILLING AK_RIGHT_PANEL_CODE 
											  golaEdgeTrimCtrlPts 						; definition of up L , down L , U cavities control points list. firsth and last points are create the line to be cut
											  GOLA_EDGE_TRIM_GEO_OFFSET_DISTANCE		; cutoff line, outward offset
											  GOLA_EDGE_TRIM_GEO_EXTENSION_DISTANCE		; extension of offseted cutoff line from start point
											  GOLA_EDGE_TRIM_GEO_EXTENSION_DISTANCE2)	; extension of offseted cutoff line to end point
					)
				)
				
				(if (equal GOLA_DRAW_PATTERN 1)
					(progn
						(_FSET (_ 'AK_RIGHT_PANEL_PDATA GolaPData))												
						(_FSET (_ 'AK_RIGHT_PANEL_EDGESTRIPS  GolaEdgeStrips))
					)
				)
			)
		)
		
		(if (or (equal AK_RIGHT_PANEL_PDATA nil) (equal AK_RIGHT_PANEL_EDGESTRIPS nil))
			(progn
				(_FSET (_ 'AK_RIGHT_PANEL_PDATA (_GENERATEPDATA AK_RIGHT_PANEL_WID AK_RIGHT_PANEL_HEI)))
			
				(_FSET (_ 'AK_RIGHT_PANEL_EDGESTRIPS (_ (_ AK_RIGHT_HIDDEN_EDGESTRIP_MAT HIDDEN_EDGESTRIP_WID)
													(_ AK_RIGHT_TOP_EDGESTRIP_MAT TOP_EDGESTRIP_WID)
													(_ AK_RIGHT_VISIBLE_EDGESTRIP_MAT VISIBLE_EDGESTRIP_WID)
													(_ AK_RIGHT_BOTTOM_EDGESTRIP_MAT BOTTOM_EDGESTRIP_WID))))
			)
		)	

; -------------------------------------------------- CREATING PANEL ---------------------------------------------------------		
		(_PANELMAIN AK_RIGHT_PANEL_CODE (_ AK_RIGHT_PANEL_PDATA AK_RIGHT_PANEL_ROT AK_RIGHT_PANEL_MAT AK_RIGHT_PANEL_THICKNESS))
		
		(_CREATEEDGESTRIPS AK_RIGHT_PANEL_CODE AK_RIGHT_PANEL_EDGESTRIPS)
		
		(_PUTLABEL AK_RIGHT_PANEL_CODE AK_RIGHT_PANEL_CODE (XSTR AK_RIGHT_PANEL_LABEL))
		(_PUTTAG AK_RIGHT_PANEL_CODE AK_RIGHT_PANEL_CODE AK_RIGHT_PANEL_TAG)
	)
)