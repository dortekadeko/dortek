(defun entOptimization ( tempPanelSSList / topEntList leftEntList frontEntList rightEntList rearEntList bottomEntList topPtList
										leftPtList frontPtList rightPtList rearPtList bottomPtList optTopEntList optLeftEntList
										optFrontEntList optRightEntList optRearEntList optBottomEntList allEntList tempEnt ent index)
										
	(setq topEntList '())
	(setq leftEntList '())
	(setq frontEntList '())
	(setq rightEntList '())
	(setq rearEntList '())
	(setq bottomEntList '())
	(setq topPtList '())
	(setq leftPtList '())
	(setq frontPtList '())
	(setq rightPtList '())
	(setq rearPtList '())
	(setq bottomPtList '())
	(setq optTopEntList '())
	(setq optLeftEntList '())
	(setq optFrontEntList '())
	(setq optRightEntList '())
	(setq optRearEntList '())
	(setq optBottomEntList '())
	(setq allEntList '())
	
	(foreach tempEnt tempPanelSSList
		(cond
			((and (equal "HOLE" (getEntPro tempEnt 8)) (equal (getnth 2 (getEntPro tempEnt 210)) 1.00000))
				(setq topEntList (append topEntList (list tempEnt)))
			) 
			((and (equal "HOLE" (getEntPro tempEnt 8)) (equal (getnth 0 (getEntPro tempEnt 210)) -1.000000))
				(setq leftEntList (append leftEntList (list tempEnt)))
			) 
			((and (equal "HOLE" (getEntPro tempEnt 8)) (equal (getnth 1 (getEntPro tempEnt 210)) -1.000000))
				(setq frontEntList (append frontEntList (list tempEnt)))
			) 
			((and (equal "HOLE" (getEntPro tempEnt 8)) (equal (getnth 0 (getEntPro tempEnt 210)) 1.000000))
				(setq rightEntList (append rightEntList (list tempEnt)))
			) 
			((and (equal "HOLE" (getEntPro tempEnt 8)) (equal (getnth 1 (getEntPro tempEnt 210)) 1.000000))
				(setq rearEntList (append rearEntList (list tempEnt)))
			) 
			((and (equal "HOLE" (getEntPro tempEnt 8)) (equal (getnth 2 (getEntPro tempEnt 210)) -1.000000))
				(setq bottomEntList (append bottomEntList (list tempEnt)))
			) 
			(T
				(setq allEntList (append allEntList (list tempEnt)))
			)
		)
	)
	
	(foreach ent topEntList
		(setq topPtList (append topPtList (list (getEntPro ent 10))))
	)
	(foreach ent leftEntList
		(setq leftPtList (append leftPtList (list (getEntPro ent 10))))
	)
	(foreach ent frontEntList
		(setq frontPtList (append frontPtList (list (getEntPro ent 10))))
	)
	(foreach ent rightEntList
		(setq rightPtList (append rightPtList (list (getEntPro ent 10))))
	)
	(foreach ent rearEntList
		(setq rearPtList (append rearPtList (list (getEntPro ent 10))))
	)
	(foreach ent bottomEntList
		(setq bottomPtList (append bottomPtList (list (getEntPro ent 10))))
	)
	
	(setq topPtList (c:optimizePoints topPtList))
	(setq i 0)
	(foreach index topPtList
		(if (equal (rem i 2) 0)
			(progn
				(setq optTopEntList (append optTopEntList (list (nth index topEntList))))
			)
		)
		(setq i (1+ i))
	)
	
	(setq leftPtList (c:optimizePoints leftPtList))
	(setq i 0)
	(foreach index leftPtList
		(if (equal (rem i 2) 0)
			(progn
				(setq optLeftEntList (append optLeftEntList (list (nth index leftEntList))))
			)
		)
		(setq i (1+ i))
	)
	(setq frontPtList (c:optimizePoints frontPtList))
	(setq i 0)
	(foreach index frontPtList
		(if (equal (rem i 2) 0)
			(progn
				(setq optFrontEntList (append optFrontEntList (list (nth index frontEntList))))
			)
		)
		(setq i (1+ i))
	)
	(setq rightPtList (c:optimizePoints rightPtList))
	(setq i 0)
	(foreach index rightPtList
		(if (equal (rem i 2) 0)
			(progn
				(setq optRightEntList (append optRightEntList (list (nth index rightEntList))))
			)
		)
		(setq i (1+ i))
	)
	(setq rearPtList (c:optimizePoints rearPtList))
	(setq i 0)
	(foreach index rearPtList
		(if (equal (rem i 2) 0)
			(progn
				(setq optRearEntList (append optRearEntList (list (nth index rearEntList))))
			)
		)
		(setq i (1+ i))
	)
	(setq bottomPtList (c:optimizePoints bottomPtList))
	(setq i 0)
	(foreach index bottomPtList
		(if (equal (rem i 2) 0)
			(progn
				(setq optBottomEntList (append optBottomEntList (list (nth index bottomEntList))))
			)
		)
		(setq i (1+ i))
	)
	
	(setq allEntList (append allEntList optTopEntList))
	(setq allEntList (append allEntList optLeftEntList))
	(setq allEntList (append allEntList optFrontEntList))
	(setq allEntList (append allEntList optRightEntList))
	(setq allEntList (append allEntList optRearEntList))
	(setq allEntList (append allEntList optBottomEntList))
)

(defun getOpRelatedEnts (tempPanelSSList currentOp / opRelatedEntsList tempEnt)
	(setq opRelatedEntsList '())
	(foreach tempEnt tempPanelSSList
		(if (equal (getEntPro tempEnt 8) (nth 0 currentOp))
			(setq opRelatedEntsList (append opRelatedEntsList (list tempEnt)))
		)
	)
	opRelatedEntsList
)

;Tm must be column major
;(setq Tm 	(_  			(_ 00  10  20  0 ) ;x
;							(_ 01  11  21  0 ) ;y
;							(_ 02  12  22  0 ) ;x
;							(_ 03  13  23  1 ) ;w
;			))
(defun matrixInversion (Tm  / TOUT inv00 inv10 inv20 inv30 inv01 inv11 inv21 inv31 inv02 inv12 inv22 inv32 inv03 inv13 inv23 inv33 det ) 
	;Column major transformation matrix inversion
	(setq inv00 (+ (* +1 (nth 1 (nth 1 Tm)) (nth 2 (nth 2 Tm)) (nth 3 (nth 3 Tm)))  (* -1 (nth 1 (nth 1 Tm)) (nth 2 (nth 3 Tm)) (nth 3 (nth 2 Tm)))  (* -1 (nth 2 (nth 1 Tm)) (nth 1 (nth 2 Tm)) (nth 3 (nth 3 Tm)))  (* +1 (nth 2 (nth 1 Tm)) (nth 1 (nth 3 Tm)) (nth 3 (nth 2 Tm)))  (* +1 (nth 3 (nth 1 Tm)) (nth 1 (nth 2 Tm)) (nth 2 (nth 3 Tm)))  (* -1 (nth 3 (nth 1 Tm)) (nth 1 (nth 3 Tm)) (nth 2 (nth 2 Tm))) )  )
	(setq inv10 (+ (* -1 (nth 1 (nth 0 Tm)) (nth 2 (nth 2 Tm)) (nth 3 (nth 3 Tm)))  (* +1 (nth 1 (nth 0 Tm)) (nth 2 (nth 3 Tm)) (nth 3 (nth 2 Tm)))  (* +1 (nth 2 (nth 0 Tm)) (nth 1 (nth 2 Tm)) (nth 3 (nth 3 Tm)))  (* -1 (nth 2 (nth 0 Tm)) (nth 1 (nth 3 Tm)) (nth 3 (nth 2 Tm)))  (* -1 (nth 3 (nth 0 Tm)) (nth 1 (nth 2 Tm)) (nth 2 (nth 3 Tm)))  (* +1 (nth 3 (nth 0 Tm)) (nth 1 (nth 3 Tm)) (nth 2 (nth 2 Tm))) )  )
	(setq inv20 (+ (* +1 (nth 1 (nth 0 Tm)) (nth 2 (nth 1 Tm)) (nth 3 (nth 3 Tm)))  (* -1 (nth 1 (nth 0 Tm)) (nth 2 (nth 3 Tm)) (nth 3 (nth 1 Tm)))  (* -1 (nth 2 (nth 0 Tm)) (nth 1 (nth 1 Tm)) (nth 3 (nth 3 Tm)))  (* +1 (nth 2 (nth 0 Tm)) (nth 1 (nth 3 Tm)) (nth 3 (nth 1 Tm)))  (* +1 (nth 3 (nth 0 Tm)) (nth 1 (nth 1 Tm)) (nth 2 (nth 3 Tm)))  (* -1 (nth 3 (nth 0 Tm)) (nth 1 (nth 3 Tm)) (nth 2 (nth 1 Tm))) )  )
	(setq inv30 (+ (* -1 (nth 1 (nth 0 Tm)) (nth 2 (nth 1 Tm)) (nth 3 (nth 2 Tm)))  (* +1 (nth 1 (nth 0 Tm)) (nth 2 (nth 2 Tm)) (nth 3 (nth 1 Tm)))  (* +1 (nth 2 (nth 0 Tm)) (nth 1 (nth 1 Tm)) (nth 3 (nth 2 Tm)))  (* -1 (nth 2 (nth 0 Tm)) (nth 1 (nth 2 Tm)) (nth 3 (nth 1 Tm)))  (* -1 (nth 3 (nth 0 Tm)) (nth 1 (nth 1 Tm)) (nth 2 (nth 2 Tm)))  (* +1 (nth 3 (nth 0 Tm)) (nth 1 (nth 2 Tm)) (nth 2 (nth 1 Tm))) )  )
	(setq inv01 (+ (* -1 (nth 0 (nth 1 Tm)) (nth 2 (nth 2 Tm)) (nth 3 (nth 3 Tm)))  (* +1 (nth 0 (nth 1 Tm)) (nth 2 (nth 3 Tm)) (nth 3 (nth 2 Tm)))  (* +1 (nth 2 (nth 1 Tm)) (nth 0 (nth 2 Tm)) (nth 3 (nth 3 Tm)))  (* -1 (nth 2 (nth 1 Tm)) (nth 0 (nth 3 Tm)) (nth 3 (nth 2 Tm)))  (* -1 (nth 3 (nth 1 Tm)) (nth 0 (nth 2 Tm)) (nth 2 (nth 3 Tm)))  (* +1 (nth 3 (nth 1 Tm)) (nth 0 (nth 3 Tm)) (nth 2 (nth 2 Tm))) )  )
	(setq inv11 (+ (* +1 (nth 0 (nth 0 Tm)) (nth 2 (nth 2 Tm)) (nth 3 (nth 3 Tm)))  (* -1 (nth 0 (nth 0 Tm)) (nth 2 (nth 3 Tm)) (nth 3 (nth 2 Tm)))  (* -1 (nth 2 (nth 0 Tm)) (nth 0 (nth 2 Tm)) (nth 3 (nth 3 Tm)))  (* +1 (nth 2 (nth 0 Tm)) (nth 0 (nth 3 Tm)) (nth 3 (nth 2 Tm)))  (* +1 (nth 3 (nth 0 Tm)) (nth 0 (nth 2 Tm)) (nth 2 (nth 3 Tm)))  (* -1 (nth 3 (nth 0 Tm)) (nth 0 (nth 3 Tm)) (nth 2 (nth 2 Tm))) )  )
	(setq inv21 (+ (* -1 (nth 0 (nth 0 Tm)) (nth 2 (nth 1 Tm)) (nth 3 (nth 3 Tm)))  (* +1 (nth 0 (nth 0 Tm)) (nth 2 (nth 3 Tm)) (nth 3 (nth 1 Tm)))  (* +1 (nth 2 (nth 0 Tm)) (nth 0 (nth 1 Tm)) (nth 3 (nth 3 Tm)))  (* -1 (nth 2 (nth 0 Tm)) (nth 0 (nth 3 Tm)) (nth 3 (nth 1 Tm)))  (* -1 (nth 3 (nth 0 Tm)) (nth 0 (nth 1 Tm)) (nth 2 (nth 3 Tm)))  (* +1 (nth 3 (nth 0 Tm)) (nth 0 (nth 3 Tm)) (nth 2 (nth 1 Tm))) )  )
	(setq inv31 (+ (* +1 (nth 0 (nth 0 Tm)) (nth 2 (nth 1 Tm)) (nth 3 (nth 2 Tm)))  (* -1 (nth 0 (nth 0 Tm)) (nth 2 (nth 2 Tm)) (nth 3 (nth 1 Tm)))  (* -1 (nth 2 (nth 0 Tm)) (nth 0 (nth 1 Tm)) (nth 3 (nth 2 Tm)))  (* +1 (nth 2 (nth 0 Tm)) (nth 0 (nth 2 Tm)) (nth 3 (nth 1 Tm)))  (* +1 (nth 3 (nth 0 Tm)) (nth 0 (nth 1 Tm)) (nth 2 (nth 2 Tm)))  (* -1 (nth 3 (nth 0 Tm)) (nth 0 (nth 2 Tm)) (nth 2 (nth 1 Tm))) )  )
	(setq inv02 (+ (* +1 (nth 0 (nth 1 Tm)) (nth 1 (nth 2 Tm)) (nth 3 (nth 3 Tm)))  (* -1 (nth 0 (nth 1 Tm)) (nth 1 (nth 3 Tm)) (nth 3 (nth 2 Tm)))  (* -1 (nth 1 (nth 1 Tm)) (nth 0 (nth 2 Tm)) (nth 3 (nth 3 Tm)))  (* +1 (nth 1 (nth 1 Tm)) (nth 0 (nth 3 Tm)) (nth 3 (nth 2 Tm)))  (* +1 (nth 3 (nth 1 Tm)) (nth 0 (nth 2 Tm)) (nth 1 (nth 3 Tm)))  (* -1 (nth 3 (nth 1 Tm)) (nth 0 (nth 3 Tm)) (nth 1 (nth 2 Tm))) )  )
	(setq inv12 (+ (* -1 (nth 0 (nth 0 Tm)) (nth 1 (nth 2 Tm)) (nth 3 (nth 3 Tm)))  (* +1 (nth 0 (nth 0 Tm)) (nth 1 (nth 3 Tm)) (nth 3 (nth 2 Tm)))  (* +1 (nth 1 (nth 0 Tm)) (nth 0 (nth 2 Tm)) (nth 3 (nth 3 Tm)))  (* -1 (nth 1 (nth 0 Tm)) (nth 0 (nth 3 Tm)) (nth 3 (nth 2 Tm)))  (* -1 (nth 3 (nth 0 Tm)) (nth 0 (nth 2 Tm)) (nth 1 (nth 3 Tm)))  (* +1 (nth 3 (nth 0 Tm)) (nth 0 (nth 3 Tm)) (nth 1 (nth 2 Tm))) )  )
	(setq inv22 (+ (* +1 (nth 0 (nth 0 Tm)) (nth 1 (nth 1 Tm)) (nth 3 (nth 3 Tm)))  (* -1 (nth 0 (nth 0 Tm)) (nth 1 (nth 3 Tm)) (nth 3 (nth 1 Tm)))  (* -1 (nth 1 (nth 0 Tm)) (nth 0 (nth 1 Tm)) (nth 3 (nth 3 Tm)))  (* +1 (nth 1 (nth 0 Tm)) (nth 0 (nth 3 Tm)) (nth 3 (nth 1 Tm)))  (* +1 (nth 3 (nth 0 Tm)) (nth 0 (nth 1 Tm)) (nth 1 (nth 3 Tm)))  (* -1 (nth 3 (nth 0 Tm)) (nth 0 (nth 3 Tm)) (nth 1 (nth 1 Tm))) )  )
	(setq inv32 (+ (* -1 (nth 0 (nth 0 Tm)) (nth 1 (nth 1 Tm)) (nth 3 (nth 2 Tm)))  (* +1 (nth 0 (nth 0 Tm)) (nth 1 (nth 2 Tm)) (nth 3 (nth 1 Tm)))  (* +1 (nth 1 (nth 0 Tm)) (nth 0 (nth 1 Tm)) (nth 3 (nth 2 Tm)))  (* -1 (nth 1 (nth 0 Tm)) (nth 0 (nth 2 Tm)) (nth 3 (nth 1 Tm)))  (* -1 (nth 3 (nth 0 Tm)) (nth 0 (nth 1 Tm)) (nth 1 (nth 2 Tm)))  (* +1 (nth 3 (nth 0 Tm)) (nth 0 (nth 2 Tm)) (nth 1 (nth 1 Tm))) )  )
	(setq inv03 (+ (* -1 (nth 0 (nth 1 Tm)) (nth 1 (nth 2 Tm)) (nth 2 (nth 3 Tm)))  (* +1 (nth 0 (nth 1 Tm)) (nth 1 (nth 3 Tm)) (nth 2 (nth 2 Tm)))  (* +1 (nth 1 (nth 1 Tm)) (nth 0 (nth 2 Tm)) (nth 2 (nth 3 Tm)))  (* -1 (nth 1 (nth 1 Tm)) (nth 0 (nth 3 Tm)) (nth 2 (nth 2 Tm)))  (* -1 (nth 2 (nth 1 Tm)) (nth 0 (nth 2 Tm)) (nth 1 (nth 3 Tm)))  (* +1 (nth 2 (nth 1 Tm)) (nth 0 (nth 3 Tm)) (nth 1 (nth 2 Tm))) )  )
	(setq inv13 (+ (* +1 (nth 0 (nth 0 Tm)) (nth 1 (nth 2 Tm)) (nth 2 (nth 3 Tm)))  (* -1 (nth 0 (nth 0 Tm)) (nth 1 (nth 3 Tm)) (nth 2 (nth 2 Tm)))  (* -1 (nth 1 (nth 0 Tm)) (nth 0 (nth 2 Tm)) (nth 2 (nth 3 Tm)))  (* +1 (nth 1 (nth 0 Tm)) (nth 0 (nth 3 Tm)) (nth 2 (nth 2 Tm)))  (* +1 (nth 2 (nth 0 Tm)) (nth 0 (nth 2 Tm)) (nth 1 (nth 3 Tm)))  (* -1 (nth 2 (nth 0 Tm)) (nth 0 (nth 3 Tm)) (nth 1 (nth 2 Tm))) )  )
	(setq inv23 (+ (* -1 (nth 0 (nth 0 Tm)) (nth 1 (nth 1 Tm)) (nth 2 (nth 3 Tm)))  (* +1 (nth 0 (nth 0 Tm)) (nth 1 (nth 3 Tm)) (nth 2 (nth 1 Tm)))  (* +1 (nth 1 (nth 0 Tm)) (nth 0 (nth 1 Tm)) (nth 2 (nth 3 Tm)))  (* -1 (nth 1 (nth 0 Tm)) (nth 0 (nth 3 Tm)) (nth 2 (nth 1 Tm)))  (* -1 (nth 2 (nth 0 Tm)) (nth 0 (nth 1 Tm)) (nth 1 (nth 3 Tm)))  (* +1 (nth 2 (nth 0 Tm)) (nth 0 (nth 3 Tm)) (nth 1 (nth 1 Tm))) )  )
	(setq inv33 (+ (* +1 (nth 0 (nth 0 Tm)) (nth 1 (nth 1 Tm)) (nth 2 (nth 2 Tm)))  (* -1 (nth 0 (nth 0 Tm)) (nth 1 (nth 2 Tm)) (nth 2 (nth 1 Tm)))  (* -1 (nth 1 (nth 0 Tm)) (nth 0 (nth 1 Tm)) (nth 2 (nth 2 Tm)))  (* +1 (nth 1 (nth 0 Tm)) (nth 0 (nth 2 Tm)) (nth 2 (nth 1 Tm)))  (* +1 (nth 2 (nth 0 Tm)) (nth 0 (nth 1 Tm)) (nth 1 (nth 2 Tm)))  (* -1 (nth 2 (nth 0 Tm)) (nth 0 (nth 2 Tm)) (nth 1 (nth 1 Tm))) )  )
	
	(setq det (+ (* (nth 0 (nth 0 Tm)) inv00 )  (* (nth 0 (nth 1 Tm)) inv10 ) (* (nth 0 (nth 2 Tm)) inv20 ) (* (nth 0 (nth 3 Tm)) inv30 ) ))
	
	(if (equal det 0)
		(setq TOUT nil)
		(progn
			(setq det (/ 1 det))
			
			(setq TOUT (_  	(_ (* inv00 det)  (* inv10 det)  (* inv20 det) (* inv30 det))
							(_ (* inv01 det)  (* inv11 det)  (* inv21 det) (* inv31 det))
							(_ (* inv02 det)  (* inv12 det)  (* inv22 det) (* inv32 det))
							(_ (* inv03 det)  (* inv13 det)  (* inv23 det) (* inv33 det))
			))
		)
	)
	TOUT
)



;Tm must be column major
;(setq Tm 	(_  			(_ 00  10  20  0 ) ;x
;							(_ 01  11  21  0 ) ;y
;							(_ 02  12  22  0 ) ;x
;							(_ 03  13  23  1 ) ;w
;			))
;v ( x y z 1)
(defun matrixVectorMult (Tm v / vout)
	(setq vout  (_ 	
						(+ (* (nth 0 (nth 0 Tm)) (nth 0 v)) (* (nth 0 (nth 1 Tm)) (nth 1 v)) (* (nth 0 (nth 2 Tm)) (nth 2 v)) (nth 0 (nth 3 Tm)) )
						(+ (* (nth 1 (nth 0 Tm)) (nth 0 v)) (* (nth 1 (nth 1 Tm)) (nth 1 v)) (* (nth 1 (nth 2 Tm)) (nth 2 v)) (nth 1 (nth 3 Tm)) )
						(+ (* (nth 2 (nth 0 Tm)) (nth 0 v)) (* (nth 2 (nth 1 Tm)) (nth 1 v)) (* (nth 2 (nth 2 Tm)) (nth 2 v)) (nth 2 (nth 3 Tm)) )
				)
	)
	
	vout
)


(defun absoluteToLocal (Tm v / )
	(matrixVectorMult (matrixInversion Tm) v)
)

(defun localToAbsolute (Tm v /) 
	(matrixVectorMult Tm v)
)

(defun vectorCross ( v1 v2 / crossProduct)
	(setq crossProduct (_ (- (* (nth 1 v1) (nth 2 v2)) (* (nth 2 v1) (nth 1 v2))) (- (* (nth 2 v1) (nth 0 v2)) (* (nth 0 v1) (nth 2 v2))) (- (* (nth 0 v1) (nth 1 v2)) (* (nth 1 v1) (nth 0 v2)))))
)
(princ)