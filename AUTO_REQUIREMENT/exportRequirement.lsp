(defun c:slayt_dortek ( / grup selList selection mlofl mdata rootPath modulCode yp1 aci)
	(init_it)
	
	(setq filePath "btoolsets\\templatefiles\\DOORTEK_REQUIREMENT.xlsx")
	
	 (if (null ad_GrupList)
        (load-GrupList)
    )
    (if (null (setq selList (getGrupsToBeSlayt ad_GrupList)))
        (exit)
    )
	
	(if (null lmm-BToolExeArgumentsTemplate_ORJ) (setq lmm-BToolExeArgumentsTemplate_ORJ lmm-BToolExeArgumentsTemplate))
										    ;1	   ;2	   ;3     ;4     ;5     ;6     ;7     ;8     ;9     ;10    ;11    ;12    ;13    ;14    ;15    ;16   ;17	    ;18
	(setq lmm-BToolExeArgumentsTemplate " \"~s\" \"~s \" \"~s\" \"~s\" \"~s\" \"~s\" \"~s\" \"~s\" \"~s\" \"~s\" \"~s\" \"~s\" \"~s\" \"~s\" \"~s\" \"~s\" \"~s\"  \"~s\"")
	
	
	(if (null lmm_writeAndSendBTool_sendXML_ORJ) (setq lmm_writeAndSendBTool_sendXML_ORJ lmm_writeAndSendBTool_sendXML))
	(defun lmm_writeAndSendBTool_sendXML (funcExeArguments  / exeParams)	
		(if (or (null wasopresultlist) (yesNo (cons (sprintf (XSTR "There are some exceptions listed below. Do you still want to export parts to ~s?") "Billtool") wasopresultlist)))	
			(progn				
				(setq bltoolPath (strcat ad_ADekoPath "billtool.EXE"))
				(princ (strcat "\n" modulCode " isimli modulun " infoString))
				(create_process (sprintf "\"~s\" ~s" (list bltoolPath funcExeArguments)) 1)
			)
		)
	)
	
	(createDir (setq rootPath (strcat (getvar "DWGPREFIX") "ADEKO REQUIREMENT\\")))
	
	(if (ssget "X" '((67 . 0)))
		(command "_.ERASE" "_ALL" "")
	)
	(foreach selection selList
        (setq mLofL (read (strcat (setq grup (car (nth (fix selection) ad_GrupList))) ":mLofL")))
		
        (if (null (eval mLofL)) (set_mLofL mLofL nil (strcat ad_DefPath grup ".def")))
		
		(princ (strcat "\n\n" (cadr (nth (fix selection) ad_GrupList))))
		
        (setq mLofL (eval mLofL))
        (foreach mdata mlofl
			(if (member (getnth 0 (getFileTimeInfo (strcat rootPath (getnth 0 mdata) "$1.xlsx"))) (list nil ""))
				(progn
					(if (not (member (getnth 1 mdata) (list 'DIVIDER "" nil)))
						(progn
							(ins2dwg mdata)
							(setq modulCode (dfetch 2 (entlast)))
							(exportRequirementToExcel)
						)
					)
				)
			)
			(if (ssget "X" '((67 . 0)))
				(command "_.ERASE" "_ALL" "")
			)
		)
	)
	
	(dos_execute (strcat "explorer.exe \"" rootPath "\""))
	(setq lmm-BToolExeArgumentsTemplate lmm-BToolExeArgumentsTemplate_ORJ)
	(setq lmm_writeAndSendBTool_sendXML lmm_writeAndSendBTool_sendXML_ORJ)
	(end_it)
)

(defun exportRequirementToExcel ( / infoString)
	;ihtiyac listesi
	(setq infoString "ihtiya� listesi hazirlaniyor...")
	
	(setq lmm-projectInfo (list (getvar "DWGNAME") "" (lmm_getDate 1) (lmm_atof 1) nil nil))
	
	(setq emptyString "" exeParams (sprintf lmm-BToolExeArgumentsTemplate 
			(list 	;1 openingExe
					"regular"   
					 ;2 DWGPREFIXFolder or itemPropertyFirstItemCode
					rootPath                
					;3 userType
					"silent" 
					;4 userGroupType
					"part2camadeko" 		
					;5 settingsGroupType					
					"reqlist"
					;6 productionProjectName or itemPropertyFirstType
					(getnth 0  lmm-projectInfo)    
					;7 productionProjectDescription					
					emptyString
					;8 productionProjectMultiple
					(convertstr (getnth 3  lmm-projectInfo)     ) 
					 ;9 database file path
					lmm-btoolMatDatabaseFullPathAsUri  
					;10 template file path					
					(lmm_getFullPathAccordingToDefOrRootPath filePath (list 3) )
					 ;11 kassa path
					emptyString     
					; 12 exclude groups
					emptyString
					; 13 outputfilename
					modulCode	
					; 14 includePrices
					lmm-BToolAllowedPrices	
					; 15 XMLOutputPath
					emptyString					
					; 16 General Options Group Type
					"part2cam"
					; 17 Default Shown price
					emptyString
					; 18 Save directly
					"yes"	
			))
	)
	(lmm_start (append lmm-projectInfo (list 5 exeParams)) )
)

(defun AK1 (kod ADEN ADH ADDer gor_GEN brutEN kap_PAY ceklist raflist lstData kapak_tip imalTipi note
            / yp1 yp2 yp3 aci ADEN1 ADEN2 LR kapak# sBazaType mdata SubEs i ADDer2 szMData kulpyer kulpyerkpkUst unitH unitW
              notch_type notch_dims dCornAng szKpmLayer blindDoorTouchWall isHbase szBodyLayer specialDoor unitCArea isManualCArea drawerRecipe bodyAuxRcpsList noBodyDrawing divDwgParamsList doorTopTrim doorTopTrimReal doorAnimateScript
				previousDrawers tempDrawer iii tempent1
				divsRafDikOffsets
				drawersListWithRcp
				drawerOffsetList
				fixedShelfList
				doorsPropList
				tempRestH
				kodFormula 
				doorAnimateScript 
				leftDwgParamsList rightDwgParamsList moduleDwgListsList noHandle coverMat  tempparamlist
				modulgolaOrderList ak1rutin zTop zBot
				ADHFormula
           )

	;lisans kontrol�
	;(isKitchenLicensed)	
	(generateKodFormula)
	
	(if (null ADH)   (setq ADH ad_ADH))
	(generateADHFormula)
	
	(setq zTop g_ClearBaseZTop zBot g_ClearBaseZBot)
    (setq dCornAng DEG_90)
    (if (null gor_GEN)
        (progn
            (initget "40 45 50")
            (setq gor_GEN (getkword (Xstr "\nDoor width 40,45,<50>: ")))
            (if (null gor_GEN) (setq gor_GEN "50"))
            (setq gor_GEN (atof gor_GEN))
            (setq brutEN (+ (convertLength 60.0 5) gor_GEN))
        )
    )

	; (setq ADHFormula ADH)	
	; (if (null ADH)   (setq ADH ad_ADH) (setq ADH (lmm_atofFormula ADH)))
	
    (if (isList lstData)
        (setq kapak# 	  (nth 0 lstData)
			  SubEs  	  (nth 1 lstData)
			  sBazaType   (nth 2 lstData)
              szKpmLayer  (nth 3 lstData)
			  LR		  (nth 4 lstData)
			  blindDoorTouchWall (nth 5 lstData)
			  specialDoor (nth 6 lstData)
			  unitCArea (nth 7 lstData)
			  isManualCArea (nth 8 lstData)
			  drawerRecipe (nth 9 lstData)
			  bodyAuxRcpsList (nth 10 lstData)
			  bodyScriptFiles (nth 11 lstData)
			  leftDwgParamsList (nth 12 lstData)
			  doorTopTrim (nth 13 lstData)
			  doorTopTrimReal (ifnull (lmm_atofFormula (nth 13 lstData)) 0.0)
			  doorAnimateScript (nth 14 lstData)
			  rightDwgParamsList (nth 15 lstData)
			  noHandle (nth 16 lstData)	
			  ak1rutin (nth 17 lstData)
        )
		(setq kapak# lstData)
    )
		
	(setq kulpyerkpkUst "K-")
	(setq isHbase (< ADH ad_UDY))
	; hControl modul durumunu kontrol eder. Eger cekmece varsa limitHbase degerini alir yoksa isHbase degerini alir.
	(setq hControl isHbase)
    (if (and ceklist (not (zerop (car ceklist))))
		(progn
			(setq limitHbase (< (- ADH (car ceklist)) ad_UDY))
			(setq hControl limitHbase)
		)
	)
	
	(setq szBodyLayer  (if isHbase "CAB_BODY_BASE" "CAB_BODY_TALL"))
	
    (if (null szKpmLayer)
    	(setq szKpmLayer szBodyLayer)
    )

    (if (null sBazaType)
        (if (minusp ADEN)
            (setq ADEN (abs ADEN) sBazaType "yok")
            (setq sBazaType "duz")
        )
    )
	(if (equal "nil" LR)
		(setq LR nil)
	)
	(setq coverMat (GetBaseUnitsDoorMaterial))
    (cond ((null ADDer)    (setq ADDer  ad_ADDer  ADDer2 ad_ADDer))
          ((isList ADDer)   (setq ADDer2 (cadr ADDer)  ADDer (car ADDer)))
          (T               (setq ADDer2 ADDer))
    )
    (if (null ADDer) (setq ADDer ad_ADDer))
    (if (null ADDer2) (setq ADDer2 ad_ADDer))
	
	;kap_pay null ise derinligin 4 cm �zerinde olsun
	(if (null kap_PAY) (setq kap_PAY (+ (convertLength 4.0 5) ADDer)))

	(setq unitH ADH unitW ADEN unitD  ADDer unitD2 ADDer2 dAltKot ad_bazaH)

	(if (and ceklist
             (not (zerop (car ceklist)))
        )	
		(if (> (car ceklist) (convertLength 25.0 5))
			(setq kulpyerkpkUst "GD")
		)
	)
	
	(setq kulpyer (if (equal noHandle "1") "0-" (if (or hControl (equal kulpyerkpkUst "GD")) "KD" "D-")))
	(setq kulpyerkpkUst (if (equal noHandle "1") "0-"  kulpyerkpkUst))
    ;(if (equal (Xstr "Left") LR) (setq ADEN1 brutEN ADEN2 kap_PAY))
    ;(if (equal (Xstr "Right") LR) (setq ADEN1 kap_PAY ADEN2 brutEN))

    (setq ADEN1 brutEN ADEN2 kap_PAY)   ;soL varsayiyoruz
    (setq szMData (convertStr
        (list kodFormula 'AK1
            ADEN
			ADHFormula
            ; (if (not (equal ADH ad_ADH)) ADH)   ;ADH
            (if (equal ADDer ADDer2)
                (if (not (equal ADDer ad_ADDer)) ADDer)  ;ADDer
                (cons 'QUOTE
                    (list (list
                        (if (not (equal ADDer ad_ADDer)) ADDer)  ;ADDer
                        (if (not (equal ADDer2 ad_ADDer)) ADDer2)  ;ADDer2
            ))))
            gor_GEN
            brutEN
            kap_PAY
            (if ceklist (cons 'QUOTE (list ceklist)))
            (if raflist (cons 'QUOTE (list raflist)))
            (cons 'QUOTE (list (setq tempparamlist (list kapak# SubEs sBazaType szKpmLayer LR blindDoorTouchWall  specialDoor unitCArea isManualCArea drawerRecipe bodyAuxRcpsList bodyScriptFiles leftDwgParamsList doorTopTrim doorAnimateScript rightDwgParamsList noHandle rutin))))
            kapak_tip
            imalTipi note
        )
    ))
    (cond (mozel_mUnit
               (setq yp1 mozel_yp1
                   aci mozel_aci
                   LR           (get_Mdata mozel_mUnit k:CModulDirection)
                   notch_type   (get_Mdata mozel_mUnit k:NotchType)
                   notch_dims   (get_Mdata mozel_mUnit k:NotchDims)
               )
               (if (member LR '("R" "saG"))
                   (setq LR "saG")
                   (setq LR "soL")
               )
               (entdel mozel_mUnit)
          )
          (notch_mUnit
               (setq yp1        (dfetch 10 notch_mUnit)
                    aci         (dfetch 50 notch_mUnit)
                    LR          (get_Mdata notch_mUnit k:CModulDirection)
                    notch_type  (get_Mdata notch_mUnit k:NotchType)
                    notch_dims  (get_Mdata notch_mUnit k:NotchDims)
               )
               (if (member LR '("R" "saG"))
                   (setq LR "saG")
                   (setq LR "soL")
               )
               (if (equal LR "soL")
                   (if (notchDlg (polar yp1 aci (- ADEN1 ADEN)) aci ADEN ADH ADDer)
                      (entdel notch_mUnit)
                      (exit)
                   )
                   (if (notchDlg (polar yp1 (- aci pi/2) ADEN1) (+ aci pi/2) ADEN ADH ADDer)
                      (entdel notch_mUnit)
                      (exit)
                   )
               )
          )
          (T
              (get_inputs "AK1" (list ADEN1 ADEN2) ADH (list ADDer ADDer2))   ;get_inputs sets LR			
			  (cond ((member LR '("r" "R")) (setq LR "saG"))
				    ((member LR '("l" "L")) (setq LR "soL"))
			  )
			  ;only controling for angle enough to understand get inputs correct well or not.
			  (if (null aci) (exit))
              (if (> g_iNotchMode 0)
                  (if (equal LR "soL")
                      (checkInterference (polar yp1 aci (- ADEN1 ADEN)) aci ADEN ADH ADDer)
                      (checkInterference (polar yp1 (- aci pi/2) ADEN1) (+ aci pi/2) ADEN ADH ADDer)
                  )
				  (progn
					(setq notch_type 0)
					(setq notch_dims (list 0 0 0))
				  )
              )
          )
    )

    (if (equal "saG" LR)
		(setq ADEN1 kap_PAY ADEN2 brutEN)
	)

    (if (equal "soL" LR)
        (akdL_motor)
        (akdR_motor)
    )
	(if (GetBaseUnitsDoorMaterial)  (set_block_material (tblobjname "BLOCK" (dfetch 2 (entlast))) T "CAB_DOORS" (GetBaseUnitsDoorMaterial)))
	; (eval (lmm_readEvenIfnil ak1rutin))
	(princ)	
)

(defun UK1 (kod UDEN UDH UDDer gor_GEN brutEN kap_PAY raflist lstData kapak_tip imalTipi note
            / yp1 aci UDEN1 UDEN2 UDDer2 LR szMData kapak# SubEs szKpmLayer blindDoorTouchWall specialDoor unitCArea isManualCArea bodyAuxRcpsList noBodyDrawing divDwgParamsList doorBottomTrim
              notch_type notch_dims dCornAng  szBodyLayer divsRafDikOffsets	doorsPropList
			  kodFormula moduleDwgListsList coverMat modulgolaOrderList uk1rutin zTop zBot
           )

	;lisans kontrol�
	;(isKitchenLicensed)
	
	(generateKodFormula)
	(generateUDHFormula)	
	
	(setq zTop g_ClearWallZTop zBot g_ClearWallZBot)
    (setq dCornAng pi/2)
    (if (and (null gor_GEN)
             (null (setq gor_GEN (getreal (Xstr "\nWidth of door <30>: "))))
        )
        (setq gor_GEN (convertLength 30.0 5))
    )

    (if (isList lstData)
        (setq kapak# 	  (nth 0 lstData)
			  SubEs  	  (nth 1 lstData)
              szKpmLayer  (nth 2 lstData)
			  LR		  (nth 3 lstData)
			  blindDoorTouchWall (nth 4 lstData)
			  specialDoor (nth 5 lstData)
			  unitCArea (nth 6 lstData)
			  isManualCArea (nth 7 lstData)
			  bodyAuxRcpsList (nth 8 lstData)
			  bodyScriptFiles (nth 9 lstData)
			  leftDwgParamsList (nth 10 lstData)
			  doorBottomTrim (nth 11 lstData)
			  doorAnimateScript (nth 12 lstData)
			  rightDwgParamsList (nth 13 lstData)
			  noHandle (nth 14 lstData)
			  uk1rutin (nth 15 lstData)			  
        )
		(setq kapak# lstData)
    )
	(setq szBodyLayer "CAB_BODY_WALL")
    (if (null szKpmLayer)
    	(setq szKpmLayer szBodyLayer)
    )
	(if (equal "nil" LR)
		(setq LR nil)
	)
	(setq coverMat (GetWallUnitsDoorMaterial))
    
    ;(if (null UDDer) (setq UDDer ad_UDDer))
    
    (cond ((null UDDer)   (setq UDDer  ad_UDDer  UDDer2 ad_UDDer))
          ((isList UDDer) (setq UDDer2 (cadr UDDer)  UDDer (car UDDer)))
          (T              (setq UDDer2 ADDer))
    )
    (if (null UDDer) (setq UDDer ad_UDDer))
    (if (null UDDer2) (setq UDDer2 ad_UDDer))

	(setq stdKulpYer (if (equal noHandle "1") "0-" "GD"))
    (setq UDEN1 brutEN UDEN2 kap_PAY)  ;soL varsayiyoruz
	(setq unitH UDH unitW UDEN unitD  UDDer unitD2 UDDer2 dAltKot 0.0)
    (setq szMData (convertStr
        (list kodFormula 'UK1 UDEN UDHFormula 
            (if (equal UDDer UDDer2)
                (if (not (equal UDDer ad_UDDer)) UDDer)  ;UDDer
                (cons 'QUOTE
                    (list (list
                        (if (not (equal UDDer ad_UDDer)) UDDer) ;UDDer
                        (if (not (equal UDDer2 ad_UDDer)) UDDer2)  ;UDDer2
            ))))
            gor_GEN brutEN kap_PAY
            (if raflist (cons 'QUOTE (list raflist)))
            ;kapak#
            (cons 'QUOTE (list (list kapak# SubEs szKpmLayer LR blindDoorTouchWall specialDoor unitCArea isManualCArea bodyAuxRcpsList bodyScriptFiles leftDwgParamsList doorBottomTrim doorAnimateScript rightDwgParamsList noHandle)))
			kapak_tip imalTipi note
        )
    ))
	
    (cond (mozel_mUnit
               (setq yp1 mozel_yp1
                   aci mozel_aci
                   LR (get_Mdata mozel_mUnit k:CModulDirection)
                   notch_type  (get_Mdata mozel_mUnit k:NotchType)
                   notch_dims  (get_Mdata mozel_mUnit k:NotchDims)
               )
               (if (member LR '("R" "saG"))
                   (setq LR "saG")
                   (setq LR "soL")
               )
               (entdel mozel_mUnit)
          )
          (notch_mUnit
               (setq yp1        (dfetch 10 notch_mUnit)
                    aci         (dfetch 50 notch_mUnit)
                    LR          (get_Mdata notch_mUnit k:CModulDirection)
                    notch_type  (get_Mdata notch_mUnit k:NotchType)
                    notch_dims  (get_Mdata notch_mUnit k:NotchDims)
               )
               (if (member LR '("R" "saG"))
                   (setq LR "saG")
                   (setq LR "soL")
               )
               (if (equal LR "soL")
                   (if (notchDlg (polar yp1 aci (- UDEN1 UDEN)) aci UDEN UDH UDDer)
                      (entdel notch_mUnit)
                      (exit)
                   )
                   (if (notchDlg (polar yp1 (- aci pi/2) UDEN1) (+ aci pi/2) UDEN UDH UDDer)
                      (entdel notch_mUnit)
                      (exit)
                   )
               )
          )
          (T
              (get_inputs "UK1" (list UDEN1 UDEN2) UDH (list UDDer UDDer2))   ;get_inputs sets LR
			  (cond ((member LR '("r" "R")) (setq LR "saG"))
				    ((member LR '("l" "L")) (setq LR "soL"))
			  )
			  (if (null aci) (exit))
              (if (> g_iNotchMode 0)
                  (if (equal LR "soL")
                      (checkInterference (polar yp1 aci (- UDEN1 UDEN)) aci UDEN UDH UDDer)
                      (checkInterference (polar yp1 (- aci pi/2) UDEN1) (+ aci pi/2) UDEN UDH UDDer)
                  )
				  (progn
					(setq notch_type 0)
					(setq notch_dims (list 0 0 0))
				  )
              )
          )
    )

    (if (equal "saG" LR)
		(setq UDEN1 kap_PAY UDEN2 brutEN)
	)
	
    (if (equal "soL" LR)
        (ukdL_motor)
        (ukdR_motor)
    )
	(if (GetWallUnitsDoorMaterial)  (set_block_material (tblobjname "BLOCK" (dfetch 2 (entlast))) T "CAB_DOORS" (GetWallUnitsDoorMaterial)))
	; (eval (lmm_readEvenIfnil uk1rutin))
	(princ)
)