(setq rout_chain_name0 "Mutfak Modeli")
(setq rout_chain_name1 "G�vde Malzemesi")
(setq rout_chain_name2 "Kapak Modeli")
(setq rout_chain_name3 "Cam Kapak Modeli")
(setq rout_chain_name4 "Alt - Boy Dolap Kapak Malzemesi")
(setq rout_chain_name5 "Aluminyum �er�eve Malzemesi")
(setq rout_chain_name6 "Cam Malzemesi")
(setq rout_chain_name7 "Kulp Modeli")
(setq rout_chain_name8 "Gola Profil Materyali")
(setq rout_chain_name9 "Baza Tipi")
(setq rout_chain_name10 "Baza Y�ksekli�i")
(setq rout_chain_name11 "Baza Malzemesi")
(setq rout_chain_name12 "�st Dolap Kapak Malzemesi")
(setq rout_chains_display_index (list 0 1 2 3 4 12 5 6 7 8 9 10 11))

(defun routchain0 ( / optionList fo currentLine)
	(if (setq fo (open (strcat AD_DEFPATH "ROUTER\\KFM\\KFM_MODELS.rout") "r"))
		(progn
			(while (setq currentLine (read-line fo))
				(setq optionList (append optionList (list (list currentLine currentLine))))	
			)
			(setq rout_sel0 (rout_fastchooser optionList "DORTEK_KITCHEN_MODEL" (Xstr "")))
			(close fo)
			(mapcar '(lambda(x) (set_tile x "")) (list "sel1" "sel2" "sel3" "sel4" "sel5" "sel6" "sel7" "sel8" "sel9" "sel10" "sel11" "sel12"))
			(mapcar '(lambda(x) (set x "")) (list 'rout_sel1 'rout_sel2 'rout_sel3 'rout_sel4 'rout_sel5 'rout_sel6 'rout_sel7 'rout_sel8 'rout_sel9 'rout_sel10 'rout_sel11 'rout_sel12))
		)
		(progn
			(alert "Mutfak Model Dosyas� Bulunamad�!")
			(set_tile "sel0" "")
			(setq rout_sel0 "")
		)
	)
)

(defun routchain1 ( / )
	(if (equal (get_tile "sel0") "")
		(progn
			(alert "Mutfak modelini belirlemeden g�vde malzemesini belirleyemezsiniz!")
		)
		(progn
			(rout_chain_seperated_layer_mat (list "CAB_BODY_BASE" "CAB_BODY_TALL" "CAB_BODY_WALL") (strcat (getxref "DORTEK_KITCHEN_MODEL") "KFM DTMBODY") 1 nil nil)
		)
	)
)

(defun routchain2 ( / ) 
	(if (equal (get_tile "sel0") "")
		(progn
			(alert "Mutfak modelini belirlemeden kapak modelini belirleyemezsiniz!")
		)
		(progn
			(rout_arrange_doors (strcat (getxref "DORTEK_KITCHEN_MODEL") "_DOOR_MODELS"))
			(rout_chain_door 2)
			(rout_arrange_doors "")
			(if removeAllVisibleSide (removeAllVisibleSide))

			(mapcar '(lambda(x) (set_tile x "")) (list "sel3" "sel4" "sel7" "sel12"))
			(mapcar '(lambda(x) (set x "")) (list 'rout_sel3 'rout_sel4 'rout_sel7 'rout_sel12))
		)
	)
)

(defun routchain3 () 
	(if (equal (get_tile "sel0") "")
		(progn
			(alert "Mutfak modelini belirlemeden cam kapak modelini belirleyemezsiniz!")
		)
		(progn
			(rout_arrange_doors (strcat (getxref "DORTEK_KITCHEN_MODEL") "_FRAME_MODELS"))
			(rout_chain_seperated_doors (list "SetGlsKapakName" "tumcamli") 3 nil nil)		
			(rout_arrange_doors "")
		)
	)
)

(defun routchain4 ( / selectedKapakMat UnitsSet ssList tempEnt)
	(if (equal (get_tile "sel2") "")
			(alert "Kapak modelini belirlemeden kapak malzemesini belirleyemezsiniz!")
		(progn
			(if (not (equal (get_tile "sel4") ""))
				(progn
					(alert "Mevcutta se�ili alt ve boy dolap kapak malzemesini de�i�tirebilmek i�in kapak modelini tekrardan belirleyiniz!")
					(set_tile "sel2" "")
					(setq rout_sel2 "")
				)
				(progn
					(redraw)
					(setmatmgrsearchfilter (strcat (getxref "DORTEK_KITCHEN_MODEL") "KFM " (get_tile "sel2") "DOOR"))
					(setq selectedKapakMat (car (choosematerial)))
					(setmatmgrsearchfilter "")
					
					(if selectedKapakMat
						(progn
							(set_tile "sel4" selectedKapakMat)
							(setq rout_sel4 selectedKapakMat)
							(SetKapakMat selectedKapakMat)
							(SetTallKapakMat selectedKapakMat)
							(grx_setmaterial "CAB_DOORS" selectedKapakMat)

							(setq ssList
								(convertSStoList (ssget "x" 
													(list
															'(0 . "INSERT")
															'(-4 . "<OR")
																'(8 . "SHELVES")
																'(8 . "PANELS_WALL")
																'(8 . "PANELS_SIDE")
															'(-4 . "OR>")
													)
												)
								)
							)
							(if ssList
								(foreach tempEnt ssList
									(if (member (get_mdata tempEnt '(620 . 1000)) (list "independent_panelwall" "independent_shelf" "independent_side"))
										(set_block_material tempEnt T (dfetch 8 tempEnt) selectedKapakMat)
									)
								)
							)

							(if (tblsearch "layer" "LSIDE") (grx_setmaterial "LSIDE" selectedKapakMat))
							(if (tblsearch "layer" "RSIDE") (grx_setmaterial "RSIDE" selectedKapakMat))
							(if (tblsearch "layer" "PLINTHS") (grx_setmaterial "PLINTHS" selectedKapakMat))
							
							(setq UnitsSet (convertSStoList (ssget "X" (list '(0 . "INSERT") '(-4 . "<OR") '(8 . "CAB_BODY_BASE") '(8 . "CAB_BODY_TALL") '(-4 . "OR>")))))
							
							(foreach baseUnit UnitsSet
								(set_block_material baseUnit T "LSIDE" selectedKapakMat)
								(set_block_material baseUnit T "RSIDE" selectedKapakMat)
								(set_block_material baseUnit T "CAB_DOORS" selectedKapakMat)
							)
						)
					)
				)
			)
		)
	)
)

(defun routchain12 ( / selectedKapakMat UnitsSet ssList tempEnt wallUnitsSet wallUnit)
	(if (equal (get_tile "sel2") "")
			(alert "Kapak modelini belirlemeden kapak malzemesini belirleyemezsiniz!")
		(progn
			(if (not (equal (get_tile "sel12") ""))
				(progn
					(alert "Mevcutta se�ili �st dolap kapak malzemesini de�i�tirebilmek i�in kapak modelini tekrardan belirleyiniz!")
					(set_tile "sel2" "")
					(setq rout_sel2 "")
				)
				(progn
					(redraw)
					(setmatmgrsearchfilter (strcat (getxref "DORTEK_KITCHEN_MODEL") "KFM " (get_tile "sel2") "DOOR"))
					(setq selectedKapakMat (car (choosematerial)))
					(setmatmgrsearchfilter "")
					
					(if selectedKapakMat
						(progn
							(set_tile "sel12" selectedKapakMat)
							(setq rout_sel12 selectedKapakMat)
							(SetUpKapakMat selectedKapakMat)
							(if (tblsearch "layer" "CORNICES_LOWER") (grx_setmaterial "CORNICES_LOWER" selectedKapakMat))
							(if (tblsearch "layer" "CORNICES_UPPER") (grx_setmaterial "CORNICES_UPPER" selectedKapakMat))
							
							(setq wallUnitsSet (convertSStoList (ssget "X" (list '(0 . "INSERT") '(8 . "CAB_BODY_WALL")))))
							
							(foreach wallUnit wallUnitsSet
								(set_block_material wallUnit T "LSIDE" selectedKapakMat)
								(set_block_material wallUnit T "RSIDE" selectedKapakMat)
								(set_block_material wallUnit T "CAB_DOORS" selectedKapakMat)
							)
						)
					)
				)
			)
		)
	)
)

(defun routchain5 ( / )
	(rout_chain_seperated_layer_mat (list "ALUMINIUM_FRAME") "DTMFRAME" 5 nil nil)
)

(defun routchain6 ( / )
	(rout_chain_seperated_layer_mat (list "CAB_DOOR_GLASS") "DTMGLASS" 6 nil nil)
)

(defun routchain7 ()
	(if (equal (get_tile "sel0") "")
		(alert "Mutfak modelini belirlemeden kulp modelini belirleyemezsiniz!")
		(progn
			(if (not (equal (get_tile "sel7") ""))
				(progn
					(alert "Mevcutta se�ilen kulp modelini de�i�tirebilmek i�in kapak modelini tekrardan belirleyiniz!")
					(set_tile "sel2" "")
					(setq rout_sel2 "")
				)
				(progn
					(loadkulps T)
					(rout_arrange_handles (strcat (getxref "DORTEK_KITCHEN_MODEL") "_HANDLE_MODELS"))
					(setq g_noActionOnModulList 1.0)
					(setq rout_sel7 (nesne_yer "kulp" (Xstr "Handle Types") nil))
				)
			)
		)
	)
)

(defun routchain8 ( / )
	(rout_chain_seperated_layer_mat (list "GOLAPROFILE") "DTMGOLA" 8 nil nil)
)

(defun routchain9 ( / optionList fo currentLine)
	(if (equal (get_tile "sel0") "")
		(alert "Mutfak modelini belirlemeden baza tipini belirleyemezsiniz!")
		(progn
			(if (setq fo (open (strcat AD_DEFPATH "ROUTER\\PLINTHS\\PLINTHMODELS\\" (getxref "DORTEK_KITCHEN_MODEL")  "_PLINTH_MODELS.rout") "r"))
				(progn
					(while (setq currentLine (read-line fo))
						(setq optionList (append optionList (list (list currentLine currentLine))))	
					)
					(close fo)
					(setq rout_sel9 (rout_fastchooser optionList "DORTEK_PLINTH_TYPE" (Xstr "")))
					(remove_Plinths)
					(mapcar '(lambda(x) (set_tile x "")) (list "sel10" "sel11"))
					(mapcar '(lambda(x) (set x "")) (list 'rout_sel10 'rout_sel11))
				)
				(progn
					(alert "Baza Tip Dosyas� Bulunamad�!")
					(set_tile "sel9" "")
					(setq  rout_sel9 "")
				)
			)
		)
	)
)

(defun routchain10 ( / optionList fo currentLine yukkseklikBaza)
	(if (equal (get_tile "sel9") "")
		(alert "Baza tipini belirlemeden baza y�ksekli�ini belirleyemezsiniz!")
		(progn
			(if (setq fo (open (strcat AD_DEFPATH "ROUTER\\PLINTHS\\PLINTHHEIGHTS\\" (get_tile "sel9")  "_PLINTH_HEIGHTS.rout") "r"))
				(progn
					(while (setq currentLine (read-line fo))
						(setq optionList (append optionList (list (list (atof currentLine) currentLine))))	
					)
					(close fo)
					
					(if (setq yukkseklikBaza (rout_choosePlinthH optionList))
						(progn
							(setq rout_sel10  (rtos yukkseklikBaza 2 0))
							(CreateXRef "PLINTH_HEI" rout_sel10)
						)
					)
					(remove_Plinths)
					(refreshDorTekAllInProject)
				)
				(progn
					(alert "Baza Y�kseklik Dosyas� Bulunamad�!")
					(set_tile "sel10" "")
					(setq  rout_sel10 "")
				)
			)
		)
	)
)

(defun routchain11 ( / selectedMat)
	(if (equal (get_tile "sel0") "")
		(alert "Mutfak modelini belirlemeden baza malzemesini belirleyemezsiniz!")
		(progn
			(if (equal (get_tile "sel9") "")
				(alert "Baza tipini belirlemeden baza malzemesini belirleyemezsiniz!")
				(progn
					(redraw)
					(setmatmgrsearchfilter (strcat (getxref "DORTEK_KITCHEN_MODEL") "KFM " (get_tile "sel9") "PLINTH"))
					(setq selectedMat (car (choosematerial)))
					(setmatmgrsearchfilter "")
					
					(if selectedMat
						(progn
							(if (tblsearch "layer" "PLINTHS") (grx_setmaterial "PLINTHS" selectedMat))

							(set_tile "sel11" selectedMat)
							(setq rout_sel11 selectedMat)
						)
					)
				)
			)
		)
	)
)

(defun c:matmgr->selective ( / selectedTemp kanattemp blkList fromEnt blk zEN tip kulp EN BOY bnam doorName tmp z_dCornAng Zbw2 Zdy ZWid yPa kulpSeri finalMode selectedLayer)		
	(setmatmgrsearchenablestate 1)
	(setmatmgrsearchfilter "")
	(defun funOnCloseAtMatManager ()
		;(setmatmgrsearchenablestate 1)
		(setmatmgrsearchfilter "")
		(setq g_matfilterSS nil)
		(setLayersOn "")
		(displayFlush) 
		(displayLiberate)			
	)	

	(defun c:matmgr ( / selectedTemp myModulCode donotRunMatmgrOrj myDoorMat selectedModul myChosenMat onSelectionError doorInfo selectedMat kanattemp doorName)
		(setq selectedTempFull  (rout_pickEntWithParent) donotRunMatmgrOrj nil onSelectionError nil)		
		(if (null (getnth 0 selectedTempFull))
			(progn
				(setq onSelectionError T)
				(if (yesno "Se�tini�iz noktada nesne yok. Malzeme sorgulamas� yapmak ister misiniz?")
					(progn
						(c:mogren)							
					)
				)
			)
			(progn	
				(setq selectedModul (last (last (getnth 0 selectedTempFull))))	
				(if (not (equal (type selectedModul) 'ENAME)) (setq selectedModul nil))
				(setq selectedTemp	(getnth 1 selectedTempFull)   selectedFace (getnth 1 selectedTempFull)   )
				(setq g_matfilterSS (ssadd selectedTemp) )
				(if (setq selectedLayer (dfetch 8 (entget selectedTemp))) (ssadd (tblobjname "LAYER" selectedLayer) g_matfilterSS))
			
			)
		)

		(cond  
			( onSelectionError
				(setmatmgrsearchfilter "")
				(setq donotRunMatmgrOrj T)	
			)
			((equal (dfetch 8 selectedFace) "CAB_DOOR_GLASS")
				(setmatmgrsearchfilter "DTMGLASS")
			)
			((equal (dfetch 8 selectedFace) "GOLAPROFILE")
				(setmatmgrsearchfilter "DTMGOLA")
			)
			((checkStrInStr "CORNICES" (dfetch 8 selectedFace))
				(setmatmgrsearchfilter (strcat (getxref "DORTEK_KITCHEN_MODEL") "KFM DTMBAND"))
			)
			((equal (dfetch 8 selectedFace) "PLINTHS")
				(setmatmgrsearchfilter (strcat (getxref "DORTEK_KITCHEN_MODEL") "KFM " (getxref "DORTEK_PLINTH_TYPE") "PLINTHS"))
			)
			((member (dfetch 8 selectedFace)   (list "CAB_BODY_BASE" "CAB_BODY_TALL" "CAB_BODY_WALL"))
				(setmatmgrsearchfilter "DTMBODY")
			)
			((and (dfetch 330 (entget selectedTemp)) (equal (dfetch 8 (entget selectedTemp)) "CAB_DOORS"))
				(setq kanattemp  (dfetch 2 (dfetch 330 selectedTemp)))							
				(if (regexMatch kanattemp "K.{3,}_+[0-9]{4}X[0-9]{4}.*")
					(progn
						(kpk:getData kanattemp 'tip 'kulp 'doorName 'EN 'BOY 'tmp 'Zbw2 'Zdy 'ZWid)
						
						(setmatmgrsearchfilter (strcat (getxref "DORTEK_KITCHEN_MODEL") "KFM " doorName "DOOR"))
						(setq myDoorMat (car (choosematerial)))
						(if myDoorMat
							(progn
								(set_block_material selectedModul T "CAB_DOORS" myDoorMat)
								(set_block_material selectedModul T "RSIDE" myDoorMat)
								(set_block_material selectedModul T "LSIDE" myDoorMat)
							)
						)
						(redraw)
						(setq donotRunMatmgrOrj T)
						
						(displayFlush) 
						(displayLiberate)
					)
					
				)				
			)
			((member (dfetch 8 selectedFace) (list "RSIDE" "LSIDE"))								
				(alert "G�r�n�r yan malzemesini de�i�tirmek i�in modulun kapak malzemesini de�i�tirin!")
				(setq donotRunMatmgrOrj T)
			)
			((and (member (dfetch 8 selectedFace) (list "PANELS_WALL")) (not (and (get_mdata selectedModul k:WPanelUsageType) (checkStrInStr "independent" (get_mdata selectedModul k:WPanelUsageType)))))
				(setmatmgrsearchfilter "DTMBACK")
			)
			((member (dfetch 8 selectedFace) (list "PANELS_SIDE" "SHELVES" "PANELS_WALL_2ND_BACK" "PANELS_WALL" "PANELS_WALL_DRAWER"))
				(setmatmgrsearchfilter "DTMPANEL")
			)
			(T
				(setmatmgrsearchfilter "")
			)
		)						
		(if (null donotRunMatmgrOrj) (c:matmgr_orj))				
	)
)

(defun  prepareRouterChainDialog (dclname /  iii tempchainname dclnameFile rout_chainCount)
	(setq iii 0 rout_chainCount 0)	
	(setq dclnameFile (open (strcat ad_AdekoPath dclname) "w"))
	(write-line (strcat " routerchainSelector: dialog {  	label=\"" (xstr "Select your rule routes") "\"; :column { " ) dclnameFile)

		
	(while  (and (if rout_chains_display_index (getnth rout_chainCount rout_chains_display_index) T) (setq iii (ifnull (getnth rout_chainCount rout_chains_display_index) iii)) (eval (read (setq tempchainname (strcat "rout_chain_name" (rtos iii 2 0))))))						
			(write-line (sprintf ":row {   :button {label=\"~s\" ; key=\"~i\"; action=\"(~s ~i)\";  fixed_width=true; width= 35;}   :edit_box {key=\"sel~i\"; is_enabled=false; width= 40; } } " 
				(list (xstr (eval (read tempchainname)) )  iii "rout_chain_run" iii iii)) dclnameFile)
			
			(setq rout_chainCount (1+ rout_chainCount) iii (1+ iii))
	)	
	
	(write-line (strcat ":text {label=\" " (Xstr "Use command SC to change these selections") "\";}") dclnameFile)
	(write-line "	} ok_only; }" dclnameFile)
	(close dclnameFile)
)

(defun refreshDorTekAllInProject ( / )
	(load (strcat ad_DefPath "ulibformul.lsp"))
	(refreshUnits "_ALL")
	(command "._REGEN")
	(displayflush)
)

(defun remove_Plinths ( / part ssPart firstPickStyle)
	(setq firstPickStyle (getvar "PICKSTYLE"))
	(setvar	"PICKSTYLE" 0)
	(setq ssPart
		(convertsstolist (ssget "x" (list	'(0 . "INSERT") 
							'(8 . "PLINTHS")
					)
		))
	)
	(setvar	"PICKSTYLE" firstPickStyle)
	
	(foreach part ssPart
		(entdel part)
	)
	(redraw)
)

(if (null c:cdoor_orj) (setq (c:cdoor_orj) (c:cdoor)))
(setq rout_chain_door_empty_index nil)
(defun rout_chain_door ( index / indexx)
	(setq g_noActionOnModulList 1.0)
	(c:cdoor_orj)
	(set (read (strcat "rout_sel" (rtos index 2 0))) (GetKapakName)) 
	(foreach indexx rout_chain_door_empty_index
			(set_tile (strcat "sel" (rtos indexx 2 0)) "")
			(set (read (strcat "rout_sel" (rtos indexx 2 0))) "") 
	)
)

(defun c:cdoor (/ edata temp mLofGrups ret flin upmode found index kNameOld nmEnt mLofL mLofLeval m secilenMat kulpName)  ;kmodelold
    (init_it)
	(setq g_noActionOnModulList 1.0)
	;lisans kontrol�
	(isKitchenWardrobeLicensed)   
	
	(if (null kapak:mLofL)  (loadKapaks))
	
	;Copy of GetKapakName to skip double foundDoorName call
	(if (setq nmEnt (dictsearch (namedobjdict) "AD_DOOR_NAME$$"))
		(setq kNameOld (cdr (assoc 2 nmEnt)))
	)	
	(setq index (ifnull (car (foundDoorName kNameOld)) 0))	
	(if (or (null kNameOld) (null (existDoorNameInAllDoors kNameOld)))
		(progn
			(setq kNameOld (caar kapak:mlofl)
				  index 0
			)
		    ;(CreateXRef "AD_DOOR_NAME$$" kNameOld)
			(SetKapakName kNameOld) 
        )
    )
	;--------
	
					
	;for (door)kapak group 			
	;ret return (1 upmode nth_kapak kapakgroupname) : upmode:  0=genel   1=�zel   2=t�m  3=�st dolaplar  4=Alt dolaplar
	
	(rout_arrange_doors "") ;-> Doortek, �zel kapak at�m senaryosunda kapak k�s�tlamas� istemedi.
	
	(if (setq ret (modulList 
					index ;(whichth kmodelold kapakgroupname:mLofL)
					ad_KapakGrupList
					ad_mLofKapakGrups)
		) 			
		(progn			
			(setq 	ad_KapakDefGrup (cadddr ret)
					mLofL (read (strcat ad_KapakDefGrup ":mLofL"))
					mLofLeval (eval mLofL))
			;not needed cause of kapak group
			;(setq flin (nth (caddr ret) kapak:mLofL))
			(setq flin (nth (caddr ret) mLofLeval))		  	 

			(setq selectedKpkModel (car flin))
			(setq g_forbiddenModules (getForbiddenModulList selectedKpkModel))
			
			(if selectedKpkModel
				(progn
					(setmatmgrsearchfilter (strcat selectedKpkModel "DOOR"))
					(setq secilenMat (car (choosematerial)))
					(setmatmgrsearchfilter "")
					
					(if (null secilenMat) 
						(progn
							(princ "\n")
							(princ "Modele uygulanacak olan materyal se�ilemedi. ��lem iptal edildi!")
							(princ "\n")
							(exit)
						)
					)
					
					(loadKulps T)
					(rout_arrange_handles (strcat "WITHDOOR_" (car flin) "_HANDLE_MODELS"))		
					(setq g_noActionOnModulList 1.0)	
					(if (null (setq kulpName (nesne_yer "kulp" (Xstr "Handle Types") "choose"))) (exit))
					
					(displayLiberate)
					(facesel_init)
					(displaySuppress)
					
					(eval (list (cadr flin) (car flin) (caddr flin) (cadddr flin) "ozel")) ;swap kod <=> tip, such as (KAPAKX DOORMODEL ....)	
				)
			)
			(displayLiberate)
			(facesel_exit)				
		)
	)
    (end_it)
)

(defun updateModelOfKapak_post_reserved (ins kpk doorName knam kmodel / )
	(if (equal "ozel" upmode)
		(progn
			(set_block_material kpk T "CAB_DOORS" secilenMat)
			(set_block_material ins T "RSIDE" secilenMat)
			(set_block_material ins T "LSIDE" secilenMat)
			
			(changeKanatsHandle kpk kulpName)
			(command "._REGEN")
		)
	)
)

(defun c:handle  () (c:cdoor)) ; -> Kulplar direk se�ilemesin. Kapak modeli - kapak rengi - kulp modei ��lemesi se�ilsin herzaman.

(c:matmgr->selective)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun firm_startup ()
		(c:sc)
)

(defun rout_chain_applier ()
	(if (findfile (strcat ad_DefPath "materials")) (setmatmgrmoduledir (strcat ad_DefPath "materials")))	
	(adeforceupdate)
	(redraw)
	(if (not (equal (parseRGT (getvar "BASEFILE") "adeko.dwg") ad_defpath))
		(setvar "BASEFILE" (strcat ad_defpath "adeko.dwg"))
	)
	
	(setq ad_bazah (atof (getxref "PLINTH_HEI")))
)

(defun virtual_doWhileNew ()
	(rout_chain_applier)
	(c:sc)
)

(setq virtual_doWhileOpen virtual_doWhileNew)

(enableRoutChainMode)
(setq c:model c:sc)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(princ)



