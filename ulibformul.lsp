(defun gm1_iLProfil (/ tempItem)
	(setq tempItem (strcat (getnth 0 (read (iniread ad_MOD-INI "golaProfiles" "gola_profileiL1"))) "_" (rtos (- __WID 36))))
)


(defun cutTwoRailGroove (grooveWid grooveDept grooveFrontDist distBetweenGroove / cutTwoRailGroove_params  )
	(setq cutTwoRailGroove_params (getnth 0 (getRailParams)) )		
	(mapcar 'set '(ptMinA  ptMaxA  ptOffSet dist dDepth dAngle dHeight ptTmpMin ptTmpMax ptMinBody ptMaxBody) cutTwoRailGroove_params) 
	
	(setq mozel_mUnit T 
		altkanal1pt  (replace (- ad_et grooveDept) 2 (polar ptOffSet (+ DEG_90 dAngle) (+ grooveFrontDist grooveWid) ) ) 
		altkanal2pt  (replace (- ad_et grooveDept) 2 (polar ptOffSet (+ DEG_90 dAngle) (+ grooveFrontDist grooveWid grooveWid distBetweenGroove) ) )
		ustkanal1pt	 (replace (- dHeight ad_et (- ad_et grooveDept) ) 2 (polar ptOffSet (+ DEG_90 dAngle) (+ grooveFrontDist grooveWid) ) )
		ustkanal2pt  (replace (- dHeight ad_et (- ad_et grooveDept)) 2 (polar ptOffSet (+ DEG_90 dAngle) (+ grooveFrontDist grooveWid grooveWid distBetweenGroove ) ))
	)
	
	(createGPanel (list (list 0.0 0.0 0.0) (list dist 0.0 0.0) (list dist (+ ad_et ad_et) 0.0) (list 0.0 (+ ad_et ad_et) 0.0)) (list 0.0 0.0 0.0 0.0) grooveWid 0.0 dAngle altkanal1pt "railGroove" "" nil "PANELPCONN")
	
	(createGPanel (list (list 0.0 0.0 0.0) (list dist 0.0 0.0) (list dist (+ ad_et ad_et) 0.0) (list 0.0 (+ ad_et ad_et) 0.0)) (list 0.0 0.0 0.0 0.0) grooveWid 0.0 dAngle altkanal2pt "railGroove" "" nil "PANELPCONN")
	
	(createGPanel (list (list 0.0 0.0 0.0) (list dist 0.0 0.0) (list dist (+ ad_et ad_et) 0.0) (list 0.0(+ ad_et ad_et) 0.0)) (list 0.0 0.0 0.0 0.0)    grooveWid 0.0 dAngle ustkanal1pt "railGroove" "" nil "PANELPCONN")
	
	(createGPanel (list (list 0.0 0.0 0.0) (list dist 0.0 0.0) (list dist (+ ad_et ad_et) 0.0) (list 0.0 (+ ad_et ad_et) 0.0)) (list 0.0 0.0 0.0 0.0)    grooveWid 0.0 dAngle ustkanal2pt "railGroove" "" nil "PANELPCONN")
	
	(princ)


)

(defun atla_ray (code note / kanalen kanalderinlik kanalonmesafe kanalarasi )
	;cutTwoRailGroove geni�
	(setq kanalen 1.4 kanalderinlik 1.1  kanalonmesafe 4.0 kanalarasi 1.0)
	(cutTwoRailGroove  kanalen kanalderinlik kanalonmesafe kanalarasi )
)

(defun cagberk_skm40 (code note  /  cagberk_skm40_params cagberk_skm40_params newPanelData tempZ)
		(setq cagberk_skm40_params (getnth 0 (getRailParams)) )		
		(mapcar 'set '(ptMinA  ptMaxA  ptOffSet dist dDepth dAngle dHeight ptTmpMin ptTmpMax ptMinBody ptMaxBody) cagberk_skm40_params) 
		
		(setq mozel_mUnit T 
			altkanal1pt  (replace (- ad_et 1.2) 2 (polar ptOffSet (+ DEG_90 dAngle) 1.9 ) ) 
			altkanal2pt  (replace (- ad_et 1.2) 2 (polar ptOffSet (+ DEG_90 dAngle) 3.0 ) )
			ustkanal1pt	 (replace (- dHeight ad_et (- ad_et 1.2) ) 2 (polar ptOffSet (+ DEG_90 dAngle) 1.25 ) )
			ustkanal2pt  (replace (- dHeight ad_et (- ad_et 1.2)) 2 (polar ptOffSet (+ DEG_90 dAngle) 3.0 ) )
		)
		
		(createGPanel (list (list 0.0 0.0 0.0) (list dist 0.0 0.0) (list dist (+ ad_et ad_et) 0.0) (list 0.0 (+ ad_et ad_et) 0.0)) (list 0.0 0.0 0.0 0.0)    0.8 0.0 dAngle altkanal1pt "railGroove" "" nil "PANELPCONN")
		
		(createGPanel (list (list 0.0 0.0 0.0) (list dist 0.0 0.0) (list dist (+ ad_et ad_et) 0.0) (list 0.0 (+ ad_et ad_et) 0.0)) (list 0.0 0.0 0.0 0.0) 0.8 0.0 dAngle altkanal2pt "railGroove" "" nil "PANELPCONN")
		
		(createGPanel (list (list 0.0 0.0 0.0) (list dist 0.0 0.0) (list dist (+ ad_et ad_et) 0.0) (list 0.0(+ ad_et ad_et) 0.0)) (list 0.0 0.0 0.0 0.0)    (- ad_et 1.2) 0.0 dAngle ustkanal1pt "railGroove" "" nil "PANELPCONN")
		
		(createGPanel (list (list 0.0 0.0 0.0) (list dist 0.0 0.0) (list dist (+ ad_et ad_et) 0.0) (list 0.0 (+ ad_et ad_et) 0.0)) (list 0.0 0.0 0.0 0.0)    (- ad_et 1.2) 0.0 dAngle ustkanal2pt "railGroove" "" nil "PANELPCONN")
		
		(princ)
)

;standart raf
(defun xmf_raf_offset (yuk y inputet) 
	(+ (* (- yuk (* inputet y)) (/ 1.0 (+ y 1.0))) inputet)
)

(defun xmf_raf_offset_list (yuk y inputet / xmf_raf_offset_result) 
	(repeat y
		(setq xmf_raf_offset_result (cons (+ (* (- yuk (* inputet y)) (/ 1.0 (+ y 1.0))) inputet) xmf_raf_offset_result))
	)
	xmf_raf_offset_result
)

;Biti� mod�l� raf aralar� e�itlemek i�in xmf_raf kullan
;x:Bu raf dolap i�inde ka��nc� 
;y:Bu dolap i�inde ka� raf var
;x ve y reelsay� olmal� 1.0 gibi 
(defun GM1_Raf (x y ) 
	(+ (* (- curDivH (* ad_et y)) (/ x (+ y (cm2cu 1.0))))  (* (- x (cm2cu 1.0)) ad_et))
)

;Biti� mod�l� dikme aralar� e�itlemek i�in kullan
;x:Bu dikme dolap i�inde ka��nc� 
;y:Bu dolap i�inde ka� dikme var
;x ve y reelsay� olmal� 1.0 gibi 
(defun GM1_Dikme (x y ) 
	(+ (* (- unitW (* ad_Et 2) (* ad_et y)) (/ x (+ y (cm2cu 1.0))))  (* (- x (cm2cu 1.0)) ad_et))
)

;Standar Cam raf
(defun GM1_CRaf (x y ) 
	(+ (*  curDivH (/ x (+ y 1.0)))  (* (- x 1.0) 0.8))
)

;Aventos i�i iki raf olursa �steki raf�n dolab�n �st i�inden mesafesi. �rnek HF 040 {H2} 
(defun avn_ust_raf ()  
	(list (* curDivH 0.31))
)

;Aventos i�i iki raf olursa alttaki raf�n dolab�n �st i�inden mesafesi 
(defun avn_alt_raf ()
	(list (* curDivH 0.6431))
)

;_______
;Tek b�l�ml� dolab�n b�l�m y�ksekli�i
(defun bh_1bd ()
	 remCTH
)

;Tek b�l�ml� dolab�n kapak y�ksekli�i
(defun kh_1bd ()
	 (- unitH ZTop ZBot)
)

;________
;4 e�it �ekmeceli dolab�n 1. b�l�m y�ksekli�i
(defun bh_4s_#1c ()
	(+ (- (kh_4s_#1c) ad_et) ZTop (* ZBtwn 0.5))
)

;4 e�it �ekmeceli dolab�n 1. kapak y�ksekli�i
(defun kh_4s_#1c ()
	(* (- unitH ZTop ZBtwn ZBtwn ZBtwn ZBot) 0.25)
)

;D�rde b�l�ml� sistemin 1 �ekmeceli 1 kapakl� dolab�n 3de 4e tekab�l eden alt b�l�m y�ksekli�i
(defun bh_4s_2+3+4#2b ()
	(- (kh_4s_2+3+4#2b) (- ad_et ZBot (* ZBtwn 0.5)))
)

;D�rde b�l�ml� sistemin 1 �ekmeceli 1 kapakl� dolab�n 3de 4e tekab�l eden alt b�l�m kapak y�ksekli�i
(defun kh_4s_2+3+4#2b ()
	(+ (* (- unitH ZTop ZBtwn ZBtwn ZBtwn ZBot) 0.75)  (* 2.0 ZBtwn))
)

;_________
;D�rte b�l�ml� sistemin 2de 4e tekab�l eden b�l�m y�ksekli�i
(defun bh_4s_1+2_#1b ()
	(+ (- (kh_4s_1+2_#1b) ad_et) ZTop (* ZBtwn 0.5))
)

;D�rte b�l�ml� sistemin 2de 4e tekab�l eden b�l�m�n kapak y�ksekli�i
(defun kh_4s_1+2_#1b ()
	(* (- unitH ZTop ZBtwn ZBot) 0.50)
)

;D�rte b�l�ml� sistemin 2de 4e tekab�l eden b�l�m�n alt b�l�m y�ksekli�i
(defun bh_4s_3+4_#2b ()
	(+ (- (kh_4s_1+2_#1b) ad_et) ZBot (* ZBtwn 0.5))
)

;D�rte b�l�ml� 3 �ekmeceli dolap orta b�lme 

(defun bh_4s_#2b ()
	(+ (kh_4s_#1c) ZBtwn)
)
;_____

;3 �ekmeceli 1 dar iki e�it �ekmeceli dolap orta b�lme y�ksekli�i
(defun bh_4s_3:8b_#1b ()
	(+ (kh_4s_3:8b_#1b) ZBtwn)
)

;3 �ekmeceli 1 dar iki e�it �ekmeceli dolap orta b�lme kapak y�ksekli�i
(defun kh_4s_3:8b_#1b ()
	(+ (* (- unitH ZTop ZBtwn ZBtwn ZBtwn ZBot) 0.375) (* ZBtwn 0.5))
)

;3 �ekmeceli 1 dar iki e�it �ekmeceli dolap alt b�lme y�ksekli�i 

(defun bh_4s_3:8b_#2b ()
	(+ (- (kh_4s_3:8b_#1b) ad_et) ZBot (* ZBtwn 0.50))
)

;_______
;D�rde b�l�ml� sistemin 4 e�it �ekmeceli ���nc� b�l�me tekab�l eden b�l�m y�ksekli�i, kapak birinci b�l�mdeki gibidir.
(defun bh_4s_#3c ()
	(+ (kh_4s_#1c) ZBtwn)
)

;D�rde b�l�ml� sistemin 4 e�it �ekmeceli d�rd�nc� b�l�me tekab�l eden b�l�m y�ksekli�i, kapak birinci b�l�mdeki gibidir.
(defun bh_4s_#4c ()
	(- (kh_4s_#1c) (- ad_et ZBot (* ZBtwn 0.5)))
)

;Ankastre F�r�n alt kapak
(defun ANK_K ()
	(+ remCTH (- (* 1.5 ad_et) ZBot (* ZBtwn 0.5) ) )
)

;____
;�st kalkar kapakl� dolap �st b�l�m 
(defun bh_2b_ust_#1b ()
	(- (+ (- (kh_2b_ust_#1b) ad_et) ZTop (* ZBtwn 0.5)) (* ad_et 0.5))
)

;�st kalkar kapakl� dolap kapa�� 
(defun kh_2b_ust_#1b ()
	(* (- unitH ZTop ZBtwn ZBot) 0.50)
)

;�st kalkar kapakl� dolap alt b�l�m 
(defun bh_2b_ust_#2b ()
	(- (+ (- (kh_2b_ust_#1b) ad_et) ZBot (* ZBtwn 0.5)) (* ad_et 0.5))
)

;____--______
;Asp. dolab� kapa��
(defun kh_1bd_asp ()
	(+ (- unitH ZTop ZBot) (cm2cu 13.9))
)

;_________
;Alt dolap sabit mod�ller i�in raf ayar�
(defun xmf_SabitAlt_Raf_Kural (inputet / temph)
	(setq temph (- ad_adh ad_et ad_et))
	(cond 
		((> ad_ADH 74.0) (list (xmf_raf_offset temph 2.0 inputet) (xmf_raf_offset temph 2.0 inputet)))
		((> ad_ADH 70.0) (list (xmf_raf_offset temph 1.0 inputet)))
		(T nil)
	)
)

;�st dolap sabit mod�ller i�in raf ayar�
(defun xmf_SabitUst_Raf_Kural (inputUDH inputet / temph)
	(setq temph (- inputUDH ad_et ad_et))
	(cond
		((> inputUDH 95.0) (list (xmf_raf_offset temph 3.0 inputet) (xmf_raf_offset temph 3.0 inputet) (xmf_raf_offset temph 3.0 inputet)))
		((> inputUDH 71.0) (list (xmf_raf_offset temph 2.0 inputet) (xmf_raf_offset temph 2.0 inputet)))
		((> inputUDH 39.0) (list (xmf_raf_offset temph 1.0 inputet)))
		(T nil)
	)
)

;GM1 mod�l y�ksekli�ine g�re rad adedi ayar�
(defun gm1_Ust_Raf_Kural (inputUDH inputet / tempH) 
	(setq temph (- inputUDH ad_et ad_et))
	(cond
		((> inputUDH (cm2cu 95.0)) (gm1_createShelfDividList tempH inputet 3.0))
		((> inputUDH (cm2cu 73.0)) (gm1_createShelfDividList tempH inputet 2.0))
		((> inputUDH (cm2cu 30.0)) (gm1_createShelfDividList tempH inputet 1.0))
		(T nil)
	)
)

(defun gm1_Alt_Raf_Kural (inputad_ADH inputet / tempH) 
	(setq temph (- inputad_ADH ad_et ad_et))
	(cond
		((> inputad_ADH (cm2cu 120.0)) (gm1_createShelfDividList tempH inputet 3.0))
		((> inputad_ADH (cm2cu 75.0)) (gm1_createShelfDividList tempH inputet 2.0))
		((> inputad_ADH (cm2cu 55.0)) (gm1_createShelfDividList tempH inputet 1.0))
		(T nil)
	)
)

;�styar�m dolap y�ksekli�i UDH2
(defun UYRM ()
	(/ ad_UDH1 2.0)
)

(defun UYRM2 ()
	(/ ad_UDH2 2.0)
)

;�styar�m dolap y�ksekli�i UDH3
(defun UYRM3 ()
	(/ ad_UDH3 2.0)
)

;�styar�m dolap y�ksekli�i UDH4
(defun UYRM4 ()
	(/ ad_UDH4 2.0)
)

;�styar�m dolap y�ksekli�i UDH5
(defun UYRM5 ()
	(/ ad_UDH5 2.0)
)

;Buzdolab� kabini BDH2
(defun BCH2 ()
	(+ ad_BDH2 ad_bazaH)
)

;Buzdolab� kabini BDH3
(defun BCH3 ()
	(+ ad_BDH3 ad_bazaH)
)

;Buzdolab� kabini BDH4
(defun BCH4 ()
	(+ ad_BDH4 ad_bazaH)
)

;Buzdolab� kabini BDH5
(defun BCH5 ()
	(+ ad_BDH5 ad_bazaH)
)

;Set �st� dolap �l��s�
(defun SETUSTU ()
	(+ (- ad_UDY ad_ADH ad_TezH ad_bazaH) ad_UDH)
)

;Set �st� dolap �l��s� UDH2 
(defun SETUSTU2 ()
	(+ (- ad_UDY ad_ADH ad_TezH ad_bazaH) ad_UDH2)
)

;Set �st� dolap �l��s� UDH3 
(defun SETUSTU3 ()
	(+ (- ad_UDY ad_ADH ad_TezH ad_bazaH) ad_UDH3)
)

;Set �st� dolap �l��s� UDH4 
(defun SETUSTU4 ()
	(+ (- ad_UDY ad_ADH ad_TezH ad_bazaH) ad_UDH4)
)

;Set �st� dolap �l��s� UDH5 
(defun SETUSTU5 ()
	(+ (- ad_UDY ad_ADH ad_TezH ad_bazaH) ad_UDH5)
)

;____________
;Boy dolap 
(defun bh_boy_#1b ()
	(- (- remCTH (- (- ad_UDY ad_Bazah (* ZBtwn 0.5) ) (* ad_et 1.5)))  g_ClearWallZBot )
)

(defun kh_boy_#1b ()
	;(+ (- remCTH (- (- ad_UDY ad_Bazah (* ZBtwn 0.5) ) (* ad_et 1.5))) (- ad_et ZBot (* Zbtwn 0.5)))
	;(- (- (+ (bh_boy_#1b) (* ad_et 1.5)) ZTop (* ZBtwn 0.5)) (* (- g_ClearWallZBot g_ClearWallZBtwn) 0.5))
	(- (+ (bh_boy_#1b) (* ad_et 1.5) ) ZTop (* Zbtwn 0.5))
)

(defun bh_boy_#2b ()
	(+ (- (- ad_UDY ad_Bazah (* ZBtwn 0.5) ) (* ad_et 1.5) ) g_ClearWallZBot )
)

(defun kh_boy_#2b ()
	;(- (- ad_UDY ad_Bazah (* ZBtwn 0.5) ) (* ZBtwn 0.5) ZBot)
	(- (+ (bh_boy_#2b) (* ad_et 1.5) ) ZBot (* Zbtwn 0.5))
)

;________
;Boy dolap �st uzun kapakl� �st b�l�m Alt b�l�m alt mod�l kapa�� gibi �al���r
(defun bh_boy2_#1b ()
	(+ (- remCTH (- (- ad_ADH (* ZBtwn 0.5) ) (* ad_et 1.5))) (- g_ClearBaseZTop g_ClearWallZBtwn))
)
(defun kh_boy2_#1b ()
;	(+ (- remCTH (- (- ad_ADH (* ZBtwn 0.5) ) (* ad_et 1.5))) (- ad_et ZBot (* Zbtwn 0.5)))
	(- (+ (bh_boy2_#1b) (* ad_et 1.5)) ZTop (* ZBtwn 0.5))
)

(defun bh_boy2_#2b ()
	(- (- (- ad_ADH (* ZBtwn 0.5) ) (* ad_et 1.5) ) (- g_ClearBaseZTop g_ClearWallZBtwn))
)

(defun kh_boy2_#2b ()
	(- (- (- ad_ADH (* ZBtwn 0.5) ) (* ZBtwn 0.5) ZBot) (- g_ClearBaseZTop g_ClearWallZBtwn))
)

;Boy F�r�n mod�l� iki �ekmeceli �ekmece b�l�m�
;�st �ekmece
(defun bh_bf2c_#2b ()
	;(- (+ (* (- (- ad_ADH (* ZBtwn 0.5)) (* ZBtwn 0.5) ZBtwn ZBot) 0.50) ZBtwn) (* ad_et 0.5))
	(- (+ (kh_bf2c_#2b) ZBtwn) (* ad_et 0.5)) 
)

(defun kh_bf2c_#2b ()
	(- (* (- ad_ADH ZTop ZBtwn ZBot) 0.50) (* (- g_ClearBaseZTop g_ClearWallZBtwn) 0.5))
)

;alt �ekmece
(defun bh_bf2c_#3b ()
	(- (+ (kh_bf2c_#2b) (* ZBtwn 0.5) ZBot) ad_et)
)

;Boydolap �� �ekemeceli mod�l�m dar �ekmecesi kapak bo�luk t�laraslar� kapa�a yedirilmi ve kapaklara payla�t�r�lm��t�r.
(defun bh_bf3c_#1b ()
	;(- (+ (* (- (- ad_ADH (* ZBtwn 0.5)) (* ZBtwn 0.5) ZBtwn ZBtwn ZBtwn ZBot) 0.25) ZBtwn) (* ad_et 0.5))
	(- (+ (kh_bf3c_#1b) ZBtwn) (* ad_et 0.5))
)

(defun bh_bf3c_#2b ()	
	 (+ (kh_bf3c_#1b) ZBtwn) 
)

(defun kh_bf3c_#1b ()
	(- (* (- (- ad_ADH (* ZBtwn 0.5)) (* ZBtwn 0.5) ZBtwn ZBtwn ZBtwn ZBot) 0.25) (* (- g_ClearBaseZTop g_ClearWallZBtwn) 0.25))
)

;Boydolap �� �ekemeceli mod�l�m alt b�y�k �ekmecesi kapak bo�luk t�laraslar� kapa�a yedirilmi ve kapaklara payla�t�r�lm��t�r.
(defun bh_bf3c_#3b ()
	(- (+ (kh_bf3c_#3b) (* ZBtwn 0.5) Zbot)  ad_et )
)
(defun kh_bf3c_#3b ()
	(- (+ (* (- (- ad_ADH (* ZBtwn 0.5)) (* ZBtwn 0.5) ZBtwn ZBtwn ZBtwn ZBot) 0.50) ZBtwn) (* (- g_ClearBaseZTop g_ClearWallZBtwn) 0.50))
)

;Boy F�r�n Mikrodalgal� mod�l �st kapak b�l�m
(defun bh_bfmd_#1b ()
	 (- remCTH (bh_bfmd_#2b)) 
)

(defun kh_bfmd_#1b ()
	(+ (- ( bh_bfmd_#1b ) Ztop (* ZBtwn 0.5) ) (* ad_et 1.5))
)

;Boy F�r�n Mikrodalga alt sabit �ekmeceli
(defun bh_bfmd_#2b ()
	(- (+ (kh_bfmd_#2b) (* ZBtwn 0.5) Zbot) (* ad_et 1.5))
)

(defun kh_bfmd_#2b ()
	(- (* (- ad_ADH ZTop ZBtwn ZBot) 0.50) (* (- g_ClearBaseZTop g_ClearWallZBtwn) 0.50))
)

;Buzdolab� kabini BDH2
(defun BCH2 ()
	(+ ad_BDH2 ad_bazaH)
)

;________
;Yar�mboy dolap y�ksekli�i
(defun YBOY ()
	(- ad_UDY ad_bazaH)
)

;Yar�mboy dolap i�i 
(defun gm1_YBOY_RAF ()
	(gm1_createShelfDividList curDivH ad_et 3.0)
)

;Yar�mboy tek kapakl� dolap
(defun bh_yboy ()
	 remCTH
)
(defun kh_yboy ()
	 (- H ZTop ZBot)
)

;_______
;Yar�mboy F�r�n Mikrodalga alt sabit �ekmeceli
;�st b�l�m sabit parca 
(defun bh_ybd_#1b ()
	(- remCTH (- (- (* ad_ADH 0.25) (* ZBtwn 0.5) ) (* ad_et 1.5)))
)

(defun kh_ybd_#1b ()
	(+ (- remCTH (- (- (* ad_ADH 0.25) (* ZBtwn 0.5) ) (* ad_et 1.5))) (- ad_et Ztop) )
)

(defun bh_ybd_#2b ()
	(- (* ad_ADH 0.25) (* ZBtwn 0.5) (* ad_et 1.5))
)

(defun kh_ybd_#2b ()
	(- (- (* ad_ADH 0.25) (* ZBtwn 0.5)) (* ZBtwn 0.5) ZBot)
)

;_______
;bardakl�kl�k
(defun Ust_Brdk_b1 (inputUDH / temph)
	(setq temph (- inputUDH ad_et ad_et ad_et (cm2cu 27.15)))
)
	
(defun Ust_Brdk_k1 (inputUDH / temph)
	(setq temph (- (- inputUDH (* ad_et 0.5) ad_et 27.15) (* ZBtwn 0.5) Ztop))
)


;;;;;;;;;;;;;;;;;;;;;;; GM1 Temel Fonksiyonlar
; b�l�m kalan� hesaplama (b�l�m numaras� ile kullan�l�r)  ;{(GM1_restH 1)}
(defun GM1_restH (divIndex) 
	(setq divHFormulation (strcat "{(GM1_restH " (rtos divIndex) ")}"))
	(if mozel_mUnit
		(rtos (gm1_calcRestH (- divIndex 1)) 2 4)
		(rtos (gm1_calcRestH_Selective (- divIndex 1) (ifnull BDH (lmm_atofFormula (getnth 3 (assoc kod mLofL)))) (ifnull divisionList (cadr (getnth 6 (assoc kod mLofL))))) 2 4)
	)
)

; kapak kalan� hesaplama (b�l�m numaras� ile kullan�l�r)  ;{(GM1_restDoorH 1)}
(defun GM1_restDoorH (divIndex)
	(setq doorHFormulation (strcat "{(GM1_restDoorH " (rtos divIndex) ")}"))
	(if mozel_mUnit
		(rtos (gm1_calcRestDoorH (- divIndex 1)) 2 4)
		(rtos (gm1_calcRestDoorH_Selective (- divIndex 1) (ifnull BDH (lmm_atofFormula (getnth 3 (assoc kod mLofL)))) (ifnull divisionList (cadr (getnth 6 (assoc kod mLofL))))) 2 4)
	)
)

;gola wyswyg scriptlerinde kullan�lmas� i�in yaz�ld�. kullan�m -> (gm1_divHei 2 0.0) -> 2. b�l�m�n �st k�sm�n�n module g�re y�ksekli�ini bulur ve bu y�ksekli�e 0.0 offsetini ekleyip geri d�nd�r�r.
(defun gm1_divHei (divNo heiOffset / totalHei divisionInfo i) 	
	(setq divNo (- divNo 1) totalHei 0.0 i 0)
	(while (>= divNo i)
		(setq totalHei (+ totalHei (lmm_atofFormula (if (equal i 0) ad_et (ifnull (cadr (getnth (- i 1) divisionList)) ad_et)))))
		(setq i (+ i 1))
	)
	(+ (- BDH totalHei) heiOffset)
)

;gm1 b�l�m y�ksekli�i de�erini fonksiyona �evir. kullan�m -> (gm1_divNoHei 2 0.0)
 (defun gm1_divNoHei (divNo)
	(if mozel_mUnit
		(lmm_atofFormula (cadr (getnth (- divNo 1) newDivs)))
		(lmm_atofFormula (cadr (getnth (- divNo 1) (ifnull divisionList (cadr (getnth 6 (assoc kod mLofL)))))))
	)
)

;gm1 mod�l y�ksekli�i de�erini fonksiyona �evir
(defun gm1_curUnitH () 
	(if mozel_mUnit 
	newH 
	(ifnull BDH (lmm_atofFormula (getnth 3 (assoc kod mLofL))))
	)
)


;;;;;;;;;;;;;;;;;;;;;;; GOLA 1
(setq 1GolaiLZBotValue nil)																															; iL Gola (�st mod�l) kapak a�a�� sark�tma de�eri -> nil ise yok.
(defun 1GolaiLZBot () (if 1GolaiLZBotValue 1GolaiLZbotValue (- 0 g_ClearWallZBot)))																	; iL Gola (�st mod�l) kapak a�a�� sark�tma hesaplamas�
(defun 1GolaiLZ (tempCurDoorH) (+ (1GolaiLZbot) g_ClearWallZBot tempCurDoorH))																		; iL Gola (�st mod�l) i�in  kapak ek toplam fonksiyonu -> mod�ldeki kapak form�l�ne 1GolaiLZBotValue toplatmak i�in kullan�l�r.
(defun 1GolaiLZ/2 (tempCurDoorH) (+ (* 0.5 (1GolaiLZbot)) g_ClearWallZBot tempCurDoorH) )															; iL Gola (�st mod�l) i�in kapak ek yar�m toplam fonksiyonu -> mod�ldeki kapak form�l�ne 1GolaiLZBotValue/2 toplatt�rmak i�in kullan�l�r.
(setq 1GolaiLZDepValue (getnth 1 (getnth 2 (read (iniread ad_MOD-INI "golaProfiles" "gola_profileiL1")))))											; iL Gola (�st mod�l) alt panel �ektirme de�eri -> Modul.ini i�inden iL1 profil derinli�ini okur
(defun 1GolaiLZDep () (if 1GolaiLZDepValue 1GolaiLZDepValue 0))																						; iL Gola (�st mod�l) alt panel �ektirme hesaplamas�
(defun ke_1GolaiLdep () (+ (* (1GolaiLZDep) 10) 0))																									; iL gola i�in alt panel derinlik �ektirme -> e�er �ektirme olmayacaksa set i�indeki default re�eteyi sil.
(defun ke_1GolaiL () (- 0 (1GolaiLZbot)))																											; iL gola uk1 uk2 kapak sark�tma (/k�rpma)

(setq 1GolaCZBot 2.0)																																; C gola kapak alttan uzatma istenen de�eri
(setq 1GolaCZTop 1GolaCZBot)
(defun 1GolaCBtwn () (- (getnth 0 (getnth 2 (read (iniread ad_MOD-INI "golaProfiles" "gola_profileC1")))) 1GolaCZBot 1GolaCZTop))					; C gola iki kapak aras� hesaplama -> Modul.ini i�inden C1 profil y�ksekli�ini okuyor.
(defun 1GolaCZDep () (getnth 1 (getnth 2 (read (iniread ad_MOD-INI "golaProfiles" "gola_profileC1")))))												; C gola profil derinli�i -> Modul.ini i�inden C1 profil derinli�ini okur
(defun 1GolaCZHei () (getnth 0 (getnth 2 (read (iniread ad_MOD-INI "golaProfiles" "gola_profileC1")))))												; C gola profil y�ksekli�i -> Modul.ini i�inden C1 profil derinli�ini okur

(setq 1GolaLZTop (1GolaCBtwn))																														; L gola kapak �stten bo�lu�u istenen de�eri
(defun 1GolaLZDep () (getnth 1 (getnth 2 (read (iniread ad_MOD-INI "golaProfiles" "gola_profileL1")))))												; L gola profil derinli�i -> Modul.ini i�inden L1 profil derinli�ini okur
(defun 1GolaLZHei () (getnth 0 (getnth 2 (read (iniread ad_MOD-INI "golaProfiles" "gola_profileL1")))))												; L gola profil y�ksekli�i -> Modul.ini i�inden L1 profil derinli�ini okur

(defun ke_1GolaLAK () (- 1golaLZTop ZTop))																											; L gola ak1 / ak2 kapak k�rpma
(defun ke_1GolaL () (- (- 1golaLZTop ad_et)))																										; L gola gm1 kapak indirme (/y�kseltme)
(defun ke_1GolaC () (- (- (1GolaCBtwn) (* ZBtwn 0.5))))																								; C Gola gm1 kapak indirme (/y�kseltme)

(defun ke_1GolaLdep () (+ (* (1GolaLZDep) 10) 0))																									; L gola i�in �st panel derinlik �ektirme -> kay�tlar� da etkiler.
(defun ke_1GolaCdep () (+ (* (1GolaCZDep) 10) 0))																									; C Gola i�in �st panel derinlik �ektirme

(setq ke_1golaZDep 2.1)																																; Gola alt mod�l extra derinle�tirme istenen de�eri
(setq ke_1golaZHvalue nil)																															; Gola alt/boy mod�l extra y�kseltme istenen de�eri -> nil ise gola kapak ile normal kapak e�it olur
(defun ke_1golaZH () (if ke_1golaZHvalue ke_1golaZHvalue (- (- ad_ADH g_ClearBaseZTop g_ClearBaseZbot) (- ad_ADH g_ClearBaseZbot 1golaLZTop))))		; Gola alt/boy mod�l extra y�kseltme hesaplamas�
(defun 1gd_ADH () (+ ad_ADH (ke_1golaZH)))																											; Gola alt Mod�l y�ksekli�i hesaplamas�
(defun 1gd_BDH () (+ ad_BDH2 (ke_1golaZH)))																											; Gola boy Mod�l y�ksekli�i hesaplamas�

;gola L, tek kapak y�ksekli�i -> b�l�m �l��s�ne g�re dinamik
(defun kh_1GolaL (divNo) 
	 (+ (- (gm1_divNoHei divNo) 1golaLZTop ZBot) (* 2 ad_et))
)
;gola LC, tek kapak y�ksekli�i -> b�l�m �l��s�ne g�re dinamik
(defun kh_1GolaLC (divNo)
	(- (+ (gm1_divNoHei divNo) ad_et) 1golaLZTop (* ZBtwn 0.5))
)
;gola C, �st kapak y�ksekli�i -> b�l�m �l��s�ne g�re dinamik
(defun kh_1GolaC_top (divNo)
	(+ (- (gm1_divNoHei divNo) ZBtwn))
)
;gola C, alt kapak y�ksekli�i -> b�l�m �l��s�ne g�re dinamik
(defun kh_1GolaC (divNo)
	(+ (- (gm1_divNoHei divNo) (- (1GolaCBtwn) (* ZBtwn 0.5))) ZBtwn)
)
;gola C, alt-son kapak y�ksekli�i -> b�l�m �l��s�ne g�re dinamik
(defun kh_1GolaC_bot (divNo)
	(+ (- (gm1_divNoHei divNo) (- (1GolaCBtwn) (* ZBtwn 0.5))) (- ad_et ZBot))
)
;gola C, alt-son kapak y�ksekli�i -> b�l�m �l��s�ne g�re dinamik
(defun kh_1GolaC_bot (divNo)
	(+ (- (gm1_divNoHei divNo) (- (1GolaCBtwn) (* ZBtwn 0.5))) (- ad_et ZBot))
)

;gola �ekmece /2 y�kseklikleri
(defun kh_1GolaDrw2 ()
	(* 0.5 (- (gm1_curUnitH) 1golaLZTop (1GolaCBtwn) zbot))
)
(defun kh_1GolaDrw2_fix ()
	(* 0.5 (- (1gd_ADH) 1golaLZTop (1GolaCBtwn) zbot))
)
(defun bh_1GolaLB1 ()
	(+ (* (- (gm1_curUnitH) 1golaLZTop (1GolaCBtwn) zbot) 0.5) (* 0.5 zbtwn) (- 1golaLZTop ad_et))
)
(defun bh_1GolaLB1_fix ()
	(+ (* (- (1gd_ADH) 1golaLZTop (1GolaCBtwn) zbot) 0.5) (* 0.5 zbtwn) (- 1golaLZTop ad_et))
)
(defun bh_1GolaLB2 ()
	(- (- (gm1_curUnitH) (* 2 ad_et)) (+ (* (- (gm1_curUnitH) 1golaLZTop (1GolaCBtwn) zbot) 0.5) (* 0.5 zbtwn) (- 1golaLZTop ad_et)))
)
(defun bh_1GolaLB2_fix ()
		(- (- (1gd_ADH) (* 2 ad_et)) (+ (* (- (1gd_ADH) 1golaLZTop (1GolaCBtwn) zbot) 0.5) (* 0.5 zbtwn) (- 1golaLZTop ad_et)))
)

;gola �ekmece /4 y�kseklikleri
(defun kh_1GolaDrw4 ()
	(* 0.5 (- (* 0.5 (- (gm1_curUnitH) 1golaLZTop (1GolaCBtwn) zbot)) zbtwn))
)
(defun kh_1GolaDrw4_fix ()
	(* 0.5 (- (* 0.5 (- (1gd_ADH) 1golaLZTop (1GolaCBtwn) zbot)) zbtwn))
)
(defun bh_1GolaDrw4B1 ()
	(- (+ (* 0.5 (- (* 0.5 (- (gm1_curUnitH) 1golaLZTop (1GolaCBtwn) zbot)) zbtwn)) 1golaLZTop (* ZBtwn 0.5)) ad_et)
)
(defun bh_1GolaDrw4B1_fix ()
	(- (+ (* 0.5 (- (* 0.5 (- (1gd_ADH) 1golaLZTop (1GolaCBtwn) zbot)) zbtwn)) 1golaLZTop (* ZBtwn 0.5)) ad_et)
)
(defun bh_1GolaDrw4b2 ()
	(+ (* 0.5 (- (* 0.5 (- (gm1_curUnitH) 1golaLZTop (1GolaCBtwn) zbot)) zbtwn)) ZBtwn)
)
(defun bh_1GolaDrw4b2_fix ()
	(+ (* 0.5 (- (* 0.5 (- (1gd_ADH) 1golaLZTop (1GolaCBtwn) zbot)) zbtwn)) ZBtwn)
)

;gola �ekmece /3 y�kseklikleri
(defun kh_GolaDrw3 ()
	 (* 0.5 (- (gm1_curUnitH) (* 0.5 (- (* 0.5 (- (gm1_curUnitH) 1golaLZTop (1GolaCBtwn) zbot)) zbtwn)) 1golaLZTop zbtwn (1GolaCBtwn) zbot))
)
(defun kh_1GolaDrw3_fix ()
	 (* 0.5 (- (1gd_ADH) (* 0.5 (- (* 0.5 (- (1gd_ADH) 1golaLZTop (1GolaCBtwn) zbot)) zbtwn)) 1golaLZTop zbtwn (1GolaCBtwn) zbot))
)
(defun bh_1GolaDrw3b2 ()
	 (- (+ (* 0.5 (- (gm1_curUnitH) (* 0.5 (- (* 0.5 (- (gm1_curUnitH) 1golaLZTop (1GolaCBtwn) zbot)) zbtwn)) 1golaLZTop zbtwn (1GolaCBtwn) zbot)) 1golaCZTop zbtwn) 1golaCZBot 1)				;1cm neden ara�t�r.
)
(defun bh_1GolaDrw3b2_fix ()
	 (- (+ (* 0.5 (- (1gd_ADH) (* 0.5 (- (* 0.5 (- (1gd_ADH) 1golaLZTop (1GolaCBtwn) zbot)) zbtwn)) 1golaLZTop zbtwn (1GolaCBtwn) zbot)) 1golaCZTop zbtwn) 1golaCZBot 1)						;1cm neden ara�t�r.
)

(defun bh_1GolaDrw3b3_fix ()
		 (- (+ (* 0.5 (- (1gd_ADH) (* 0.5 (- (* 0.5 (- (1gd_ADH) 1golaLZTop (1GolaCBtwn) zbot)) zbtwn)) 1golaLZTop zbtwn (1GolaCBtwn) zbot)) 1golaCZTop zbtwn) 1golaCZBot 1)					;1cm neden ara�t�r. Bulana yemek :)
)

;boy gola i�in parametreleri
(setq 1GolaZFSValue 0)																																; boy cihaz dolap sabit raf kapak bo�lu�u de�eri
(defun 1GolaZFS () (if 1GolaZFSValue 1GolaZFSValue 0))																								; boy cihaz dolap sabit raf kapak bo�lu�u hesaplama
(defun ke_1GolaZFS () (if (1GolaZFS) (- ad_et (1GolaZFS)) (* 0.5 (- ad_et zbtwn))))																	; boy cihaz dolap sabit raf kapak kald�rma

(defun bh_1GolaFSCBot () (+ (- (1GolaCBtwn) (* 0.5 zbtwn)) kh_integratedMikroDoor))
(defun bh_1GolaCboyBot () (+ (1gd_ADH) (- ad_et)))

(defun kh_1golaBoy (divNo)
	(+ (gm1_divNoHei divNo) (* 2 ad_et) (- 0 (1GolaZFS) ztop))
)

(defun kh_1golaFSBoy (divNo)
	(+ (gm1_divNoHei divNo) (* 2 ad_et) (- 0 (1GolaZFS) ztop) 1GolaCZBot)
)

(defun kh_1GolaCFS_top (divNo)
	(+ (+ (- (gm1_divNoHei divNo) ZBtwn)) (- ad_et (1GolaZFS)) (* 0.5 zbtwn))
)

(defun bh_1GolaFSDrw4b2_fix ()
	(- (bh_1GolaDrw4b2_fix) (- ad_et (1GolaZFS)) (* 0.5 zbtwn))
)

(defun bh_1GolaFSDrw4b3_fix ()
	(- (bh_1GolaDrw4b3_fix) (- ad_et (1GolaZFS)) (* 0.5 zbtwn))
)


;gola profil parametrik fix y�kseklik de�erleri -> Normalde profiller b�l�mlere ba�lanabiliyor. Bu y�kseklikler boy dolaplara �zel olarak kullan�labilir.
(defun ph_1GolaL () (gm1_curUnitH))								; L gola 1 kapakl� profil y�ksekli�i
(defun ph_1GolaL_fix () (1gd_ADH))								; L gola 1 kapakl� profil y�ksekli�i -> fix (1gd_ADH)
(defun ph_1golaCdrw2 () (+ (bh_1GolaLB2) ad_et))				; C gola 2 �ekmece profil y�ksekli�i
(defun ph_1golaCdrw2_fix () (+ (bh_1GolaLB2_fix) ad_et))		; C gola 2 �ekmece profil y�ksekli�i -> fix (1gd_ADH)
(defun ph_1golaCdrw3 () (+ (bh_1GolaDrw3b2) ad_et))				; C gola 3 �ekmece profil y�ksekli�i
(defun ph_1golaCdrw3_fix () (+ (bh_1GolaDrw3b2_fix) ad_et))		; C gola 3 �ekmece profil y�ksekli�i -> fix (1gd_ADH)
(defun ph_1golaCdrw4 () (+ (bh_1GolaDrw4b2) ad_et))				; C gola 4 �ekmece profil y�ksekli�i
(defun ph_1golaCdrw4_fix () (+ (bh_1GolaDrw4b2_fix) ad_et))		; C gola 4 �ekmece profil y�ksekli�i -> fix (1gd_ADH)



;;eski gola k�t�phane fonksiyonlar� -> art�k i�levsel de�il. gerekirse a�.
;(defun GolaUstAra () -1.7)
;(defun 2CekGolaAra () -3.35)
;(defun 3CekGolaAra () -1.75)
;(defun kh_1bdGl () (- unitH ZTop ZBot 3.2))
;(defun kh_4s_1+2_#1bGl () (- (kh_4s_1+2_#1b) 3.2))
;(defun kh_4s_#1cGl () (+ (kh_4s_#1c) (GolaUstAra) 0.1))

;mikro ve f�r�n oparametreleri
(setq bh_integratedOven 58.5)									; f�r�n b�l�m� i� bo�luk
(setq bh_integratedMikro 38)									; mikro b�l�m� i� bo�luk
(setq kh_integratedMikroDoor 5.00000001)						; mikro b�l�m�ne klapa y�ksekli�i

(setq applianceBackOffsetValue 5.0)								; boy cihaz arka havaland�rma i�in kanal�n ba�layaca�� yer - hen�z implemente edilmedi
(setq applianceBackOffsetTOP applianceBackOffsetValue)			; �st havaland�rma
(setq applianceBackOffsetBOT applianceBackOffsetValue)			; alt havaland�rma


;;;;;;;;;;;;;;;;;;;;;;; KENARBANT
(setq PvcKoduKAPAK "PVC08")		;GENEL KAPAK BANDI �SM�
(setq PvcKoduGENEL "PVC08")		;GENEL KENAR BANDI �SM�
(setq PvcKoduUST "PVC08")		;UST TARAFLAR
(setq PvcKoduALT "PVC08")		;ALT TARAFLAR
(setq PvcKoduYAN "PVC08")		;YAN TARAFLAR
(setq PvcKoduON "PVC08")		;ON TARAFLAR
(setq PvcKoduARKA "PVC08")			;ARKA TARAFA BAKAN
(setq PvcKoduCONN nil)			;BA�LAYICI GELEN YERE BANT �SM�
(setq PvcKodu45CONN nil)		;45 B�RLE��M GELEN YERE BANT �SM�

(setq PvcKoduHRafYan "PVC08")		;HAREKETL� RAF YAN TARAFLAR
(setq PvcKoduHRafArka "PVC08")	;HAREKETL� RAF ARKA TARAFLAR
(setq PvcKoduCekUst "PVC08")		;CEKMECE UST TARAFLAR
(setq PvcKoduCekAlt nil)		;CEKMECE ALT TARAFLAR