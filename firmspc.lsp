(load (strcat ad_DefPath "firmspcPOST.lsp"))

(defun prepareJsonCodesCSV ( / fo satir indexx tableMainElements elements maintable recentMem memIndex newMem CodeSubstitudeJSON_CSV memberOfTable)
	(if (setq fo (open (strcat ad_DefPath "CodeSubstitude.csv") "r"))
		(progn
			(princ (XSTR "\nReading CodeSubstitude.csv Please wait...\n"))
			
			(setq indexx 0)
			(while (setq satir (read-line fo))
				(setq elements (splitstr (lmm_replaceStrs satir (list ";;") (list ";$&$;")) ";"))
				(if (equal indexx 0) 
					(progn
						(setq tableMainElements elements)
					)
					(progn
						(if (setq recentMem (assoc (car elements) maintable))
							(progn
								(setq memIndex (- (length maintable) (length (member recentMem maintable))))
								(setq newMem (append recentMem (list (removeat 0 elements))))
								(setq maintable (replace newMem memIndex maintable))
							)
							(progn
								(setq mainTable (append mainTable (list (list (car elements) (removeat 0 elements)))))
							)
						)
					)
				)
				(setq indexx (+ indexx 1))
			)
			(princ (XSTR "\nDone.\n"))
			(close fo)
		)
	)
	
	(setq CodeSubstitudeJSON_CSV (strcat ad_DefPath "CodeSubstitudeJSON.data"))
	(lmm_deleteSelectedAuxRcpFile CodeSubstitudeJSON_CSV)
	(if (setq fo (open CodeSubstitudeJSON_CSV "append"))
		(progn
			(princ (XSTR "\nPreparing codes. Please wait...\n"))
			
			(foreach memberOfTable mainTable
				(princ (strcat (car memberOfTable) "$$" (prepareJsonCodesCSV:createJSONFormat memberOfTable) "\n") fo)
			)
			(princ (XSTR "\nDone.\n"))
			(close fo)
		)
	)
)

(defun prepareJsonCodesCSV:createJSONFormat (data / startString endString subData singleData singleDataIndex middleSubString middleString subDataIndex)
	(setq startString (strcat "{\"" (car data) "\":[") endString "]}")
	
	(setq subDataIndex 1 middleString "")
	(foreach subData (removeat 0 data)
		(setq singleDataIndex 1 middleSubString "")
		(foreach singleData subData
			(if (not (equal singleData "$&$"))
				(setq middleSubString (strcat middleSubString "\"" (getnth singleDataIndex tableMainElements) "\":\"" singleData "\"" (if (and (not (member (getnth (+ 1 singleDataIndex) subData) (list "$&$" nil))) (equal (getnth singleDataIndex subData) "$&$")) ", " (if (not (member (getnth singleDataIndex subData) (list "$&$" nil))) ", " " "))))
			)
			(setq singleDataIndex (+ singleDataIndex 1))
		)
		(setq middleString (strcat middleString "{ " middleSubString (if (equal (length (removeat 0 data)) subDataIndex) "}" "},")))
		(setq subDataIndex (+ subDataIndex 1))
	)
	(strcat startString middleString endString)
)

(defun loadSubstitudeTable ( / fo satir satirData adekoCode JsonString)
	(if (setq fo (open (strcat ad_DefPath "CodeSubstitudeJSON.data") "r"))
		(progn
			(princ (XSTR "\nLoading substitude table. Please wait...\n"))
			 (setq g_CodeSubstitudeTable (list ))
			 
			 (while (setq satir (read-line fo))	
				(setq satirData (splitstr satir "$$"))
				(if (equal (length satirData) 2) ;kod-JsonString
						(setq adekoCode (car satirData) JsonString (cadr satirData))
						(setq adekoCode nil JsonString nil)
				)
				
				(if (and adekoCode JsonString) (setq g_CodeSubstitudeTable (append g_CodeSubstitudeTable (list (list adekoCode JsonString)))))
			)
			
			(princ (XSTR "\nDone.\n"))
			(close fo)
		)
	)
	(princ)
)

(defun prepareJSONCodeCSVIfNewer (/ csvFileWriteTime csvFileLastWriteTime )
	(setq csvFileWriteTime (getnth 2 (getFileTimeInfo (strcat ad_DefPath "CodeSubstitude.csv"))))
	;read last modified date from ini if exists
	(setq csvFileLastWriteTime (iniread ad_MOD-INI "lmm-Sets" "lmm_CodeSubstitudecsvLastWriteTime" ""))
	(if (not (equal csvFileLastWriteTime csvFileWriteTime))
		(progn
			(prepareJsonCodesCSV)
			(iniwrite ad_MOD-INI "lmm-Sets" "lmm_CodeSubstitudecsvLastWriteTime" csvFileWriteTime)
		)
	)
)

(defun purgeAndwriteBillItemsToXML ( / )
	(purgeUBlocks)
	(if (tblobjname  "LAYER" "{GIZLI-CEPHE}")
		(progn
			(setvar "TILEMODE" 1)
			(gorunus (Xstr "PLan")) ;Modul layerlar�n� orjinaline set etmek i�in konuldu.
		)
	)

	(lmm_createBilltempUnitLISPXMLGROUPADDS)
	(lmm_createBilltempUnitLISPXMLADDS)
	(writeBillItemsToXML)
	
	(if (not (equal outPutDialogResult 100)) ;"CRM'e G�nder" ile geldiyse girme. ��nk� halihaz�rda kendi i�inde yap�yor bunu.
		(if g_CodeSubstitudeTable (reCreateXmlWithJSON (strcat ad_ADekoPath "billtemp" (iniread ad_MOD-INI "dataextract" "extension" ".xml"))))
	)
)

(defun lmm_writeAndSendCRM_createOutPutXMLFile ( xmlMode outputType / outFilereadFile  temptxt currentLine billtempExtension)
	(setq billtempExtension (iniread ad_MOD-INI "dataextract" "extension" ".xml"))
	(moveFile (strcat ad_ADekoPath "billtemp" billtempExtension) (strcat ad_ADekoPath "billtemp_orj.xml") T T)	
	
	(setq readFile (open (strcat ad_ADekoPath "billtemp_orj.xml") "r"))
	(setq outFile (open (strcat ad_ADekoPath "billtemp" billtempExtension) "w"))
	(while (setq currentLine (read-line readFile) )
			(if (not (equal currentLine "</PROJECT>") )
				(progn
					(write-line currentLine outFile) 
				)
			)		
	)
	(close readFile)
	(lmm_wasop_sawmap outFile outputType (list xmlMode))		
	(lmm_writeAndSendBTool_writeProjectInfo outputType)
	(write-line "</PROJECT>" outFile)
	(close outFile)	
	
	(if g_CodeSubstitudeTable (reCreateXmlWithJSON (strcat ad_ADekoPath "billtemp" billtempExtension)))
	
	(movefile (strcat ad_adekopath "billtemp" billtempExtension) (strcat ad_adekopath "billtemp_p2c.xml") t t)
	(setq dwgName (parseX (getvar "DWGNAME") ".dwg"))
	(movefile (strcat ad_adekopath "billtemp" billtempExtension) (strcat (lmm_prepareCRMOutDirs) (lmm_getDictValue "DWGPROJECTID") "(" dwgName ")" ".xml") t t)
	(lmm_deleteSelectedAuxRcpFile (strcat ad_ADekoPath "billtemp_orj.xml"))
)

(defun lmm_writeAndSendBTool_createOutPutXMLFile ( xmlMode outputType / outFile readFile outFile)
	(setq temptxt (strcat ad_ADekoPath "billtemp_p2c.xml"))		
	(setq outFile (open temptxt "w"))
	(write-line "<?xml version=\"1.0\" encoding=\"iso-8859-9\" ?>" outFile)	
	(write-line "<PROJECT>" outFile)	
	(lmm_wasop_sawmap outFile outputType (list xmlMode))		
	(lmm_writeAndSendBTool_writeProjectInfo outputType)
	(write-line "</PROJECT>" outFile)
	(close outFile)
	
	(if g_CodeSubstitudeTable (reCreateXmlWithJSON (strcat ad_ADekoPath "billtemp_p2c.xml")))
)

(defun reCreateXmlWithJSON (sourceXml / tempXml readFile outFile currentLine)
	(setq tempXml (strcat sourceXml "tmp"))
	(moveFile sourceXml tempXml T T)
	(lmm_deleteSelectedAuxRcpFile sourceXml)
	
	(setq readFile (open tempXml "r"))
	(setq outFile (open sourceXml "w"))
	(while (setq currentLine (read-line readFile) )
		(setq currentLine (putJSONStringIfExistKey currentLine))
		(write-line currentLine outFile) 		
	)
	(close readFile)
	(close outfile)
	
	(lmm_deleteSelectedAuxRcpFile tempXml)
)

(defun putJSONStringIfExistKey (xmlLine / key_json outputString)
	(if (setq key_json (assoc (parselft (parsergt xmlLine ">") "<") g_CodeSubstitudeTable))
		(setq outputString (strReplace xmlLine (car key_json) (cadr key_json)))
		(setq outputString xmlLine)
	)
	outputString
)

(defun lmm_wasop_sawmap_assayITEMs_writeForBtool (outFile)
	(write-line "<PART2CAM_ITEMS>" outFile)	
	(write-line
		(strcat 	
					"\t<GROUP>PART2CAM_ITEMS</GROUP>\n"
					"\t\t\t\t\t\t\t\t\t<CODE>" stockCode "</CODE>\n"
					"\t\t\t\t\t\t\t\t\t<UNIQUECODE>" comCode "</UNIQUECODE>\n"
					"\t\t\t\t\t\t\t\t\t<PARENT>" comParent "</PARENT>\n"
					"\t\t\t\t\t\t\t\t\t<MODUL>" comParentModul "</MODUL>\n"
					"\t\t\t\t\t\t\t\t\t<MODULPURECODE>" (parse$ comParentModul) "</MODULPURECODE>\n"
					"\t\t\t\t\t\t\t\t\t<MODULDEF>" tempComModulDef "</MODULDEF>\n"
					"\t\t\t\t\t\t\t\t\t<MODULKCODE>" comParentModulKitchenCode "</MODULKCODE>\n"
					"\t\t\t\t\t\t\t\t\t<MODULKPURECODE>" (parse$ comParentModulKitchenCode) "</MODULKPURECODE>\n"
					"\t\t\t\t\t\t\t\t\t<MODULKPOSE>" comParentModulKitchenPose "</MODULKPOSE>\n"
					"\t\t\t\t\t\t\t\t\t<MODULKGROUPPOSE>" comParentModulKitchenGroupPose "</MODULKGROUPPOSE>\n"
					"\t\t\t\t\t\t\t\t\t<TYPE>" comType "</TYPE>\n"
					"\t\t\t\t\t\t\t\t\t<QUANTITY>" (convertstr (+ (* (ifnull comQuantity 1) (ifnull lmm-ProjectMultiple 1) (ifnull lmm-FirmFactor 1) (if (equal lmm-s-useOverconsumption "1") (lmm_getOverCons comCode) 1)) (if (equal lmm-s-useOverconsumptionQuantity "1") (lmm_getOverConsQuantity comCode) 0.0) )) "</QUANTITY>\n"											
					"\t\t\t\t\t\t\t\t\t<QUANTITY_UNIT>" comQuantityUnit "</QUANTITY_UNIT>\n"
					"\t\t\t\t\t\t\t\t\t<ITEMPROJECT>" (ifnull lmm-ProjectName "") "</ITEMPROJECT>\n"
					"\t\t\t\t\t\t\t\t\t<LABEL>" labelStr "</LABEL>\n"
					"\t\t\t\t\t\t\t\t\t<TGSTR1>" tag1Str "</TGSTR1>\n"
					"\t\t\t\t\t\t\t\t\t<TGSTR2>" tag2Str "</TGSTR2>\n"
					"\t\t\t\t\t\t\t\t\t<MODELTG>" modeltagStr "</MODELTG>\n"
					"\t\t\t\t\t\t\t\t\t<COSTTYPETG>" costtypetagStr "</COSTTYPETG>\n"
					(ifnull tagXMLStrNonParameter "")
		)	outFile
	)
	
	(write-line "</PART2CAM_ITEMS>" outFile)	
)

(defun lmm_createBilltempUnitLISPXMLGROUPADDS ()
	(analyzeKitchenForXML)
)

(defun analyzeKitchenForXML ( / modulListt)
	(princ)
	(setq modulListt (lmm_getOnlyKitchenModuls))
	(setq G_LISPGLOBALXMLADDS nil)	
	
	(if modulListt (setq G_LISPGLOBALXMLADDS (strcat (createGOLAProfiles modulListt "GOLAPROFILE") "\n" (createGOLAProfiles modulListt "GOLAVERTICALPROFILE"))))
	(princ)
)

(defun mergeGOLAProfiles (ssmodullistt golaLayer / golaFlist startPoint angleProfil profilWid profilCode profilHei profilList possibleLength existProfile endList)
	(command "_.UNDO" "_M")
	(command "_.EXPLODE" ssmodullistt)
	(setq golaFlist (ssget "x" (list (cons 8 golaLayer))))
	(setq startPointsList nil)
	(setq endPointsList nil)
	(setq profilHeiList nil)
	(setq profilList nil)
	(foreach golaProfil (convertsstolist golaFlist)			
		(setq startPoint (getEntpro golaProfil 10))
		(setq angleProfil (getEntpro golaProfil 50))
		(setq profilWid (get_mdata golaProfil k:WID))
		
		(if profilWid 
			(progn 
				(setq profilCode (get_mdata golaProfil k:WPanelUsageType))
				(setq startpoint (mapcar '(lambda(x) (round x 1) ) startpoint)) 
				(setq endPoint (polar startPoint angleProfil  profilWid))		
				(setq endPoint (mapcar '(lambda(x) (round x 1) ) endPoint)) 
				(setq profilHei (get_mdata golaProfil k:HEI))
				
				(setq profilList (append profilList (list (list profilCode (if (> (setq possibleLength (distance startpoint endPoint)) profilHei) possibleLength profilHei)))))
			)
		)
	)

	(foreach profilee profilList
		(if (setq existProfile (assoc (car profilee) endList))
			(setq endList (replace (list (car profilee) (+ (cadr existProfile) (cadr profilee))) (getItemIndex existProfile endList) endList))
			(setq endList (append endList (list profilee)))
		)
	)
	
	(command "_.UNDO" "_BA")
	endList
)

(defun dogrusalmi (pointList / h1 h2 k1 k2)
	(setq h1 (- (car (cadr pointList)) (car (car pointList))))
	(setq h2 (- (car (cadddr pointList)) (car (car pointList))))
	
	(setq k1 (- (cadr (cadr pointList)) (cadr (car pointList))))
	(setq k2 (- (cadr (cadddr pointList)) (cadr (car pointList))))
	(equal (* k1 h2) (* k2 h1))
)

(defun createGOLAProfiles (ssmodullistt layerName / golasList golaa funTemp golaMat boySabiti tamBoy)
	(setq golasList (mergeGOLAProfiles ssmodullistt layerName) funresult "")
	(setq golaMat (if (and (tblobjname "LAYER" layerName) (setq golaMat (car (getmatname (tblobjname "LAYER" layerName))))) golaMat ""))
	(foreach golaa golasList
		(setq boySabiti 4.0)
		(setq golaLength (convertLengthToSpecifiedUnit (cadr golaa) (getvar "INSUNITS") 6))
		(setq tamBoy (+ (fix (/ golaLength boySabiti)) (if (equal (rem golaLength boySabiti) 0.0) 0.0 1.0)))
		(setq funTemp (sprintf "<GOLAPROFILE>\n<GROUP>GOLAPROFILE</GROUP>\n<CODE>~s</CODE>\n<UNIQUECODE>~s</UNIQUECODE>\n<MATERIAL>~s</MATERIAL>\n<QUANTITY>~f</QUANTITY>\n<DMOD_DMAT>~s</DMOD_DMAT>\n<UNITNOTE>~s</UNITNOTE>\n</GOLAPROFILE>\n" (list (car golaa) (car golaa)  golaMat tamBoy (strcat (getKapakname) (substr golaMat 1 2)) "Gola profil")))
		(setq funresult (strcat funTemp funresult))
	)
	funresult
)

(defun createGolaHandle ( / )
	(_ITEMMAIN (_& (list "GOLAPROFIL_" __WID)) (_& (list "GOLAPROFIL_" __WID)) (list 1 "ad"))
	(_PUTTAG (_& (list "GOLAPROFIL_" __WID)) (_& (list "GOLAPROFIL_" __WID)) "URETIM")
)

(setq _EDGEMAIN nil)
(defun _EDGEMAIN (edgeStripCode panelCode Parameters / edgestripEnt currentModulCode uniqueCode uniqueParentCode polylineData tempMaterial Width tempContinue)
	(setq tempContinue T)
	(if (and (or (equal lmm-outPutType 2) (equal lmm-outPutType 3)) (equal lmm-s-drawModeShowEdgeStrips "0"))
		(setq tempContinue nil)	
	)
	; (if (and tempContinue (> lmm-outPutType 0) (not (equal (getnth 2 Parameters) 0.0)))
	(if (or (and tempContinue (> lmm-outPutType 0) (not (equal (getnth 2 Parameters) 0.0))) 
			(and tempContinue (equal lmm-outPutType 0) (equal lmm-s-includeEdgesInDxf "1") (not (equal (getnth 2 Parameters) 0.0)))
			)
		(progn
			(mapcar 'set '(polylineData tempMaterial Width) Parameters)
			
			(if (equal (getnth 1 polylineData) 0.0)
				(createItem "1 04 01 092" (list (* 0.005 (/ (distance (getnth 0 polylineData) (getnth 2 polylineData)) 1000.0)) "kg"))
			)
			
			(setq edgestripEnt (lmm_drawPolyline polylineData "LMM_EDGESTRIP" "nil" ))
			(changeEntPro edgestripEnt 39 (ifnull (if (equal (convertstr (type Width)) "STR") (lmm_string2Value Width) Width) 0.0))
			(lmm_createObjsLMMDATA edgestripEnt edgeStripCode panelCode nil "EDGESTRIP" (list 1 1))
		)
	)
)

(setq _PANELMAIN nil)
(defun _PANELMAIN (panelCode Parameters / panelEnt panelEntMW tempPolylineData tempPolylineDataMW tempBBoxMW tempgrainDegree tempgrainRadian tempMaterial tempDepth tempThicknesscurrentModulCode uniqueCode uniqueParentCode tempBiesseLayerList tempReadLayer tempPolEnt tempRotations tempmainRotation tempmirrorMode tempcutListRotate tempDXFRotate uzunluk1 uzunluk2 parcaM2 matTwoChar)
	(mapcar 'set '(tempPolylineData tempRotations tempMaterial tempDepth) Parameters)	
	
	;biz a�� olarak al�yoruz radyana �evirelim
	(if (isList tempRotations)
		(progn
			(mapcar '(lambda (x y) (set x (if (null y) nil (d2r y)))) '(tempmainRotation tempcutListRotate tempDXFRotate) tempRotations)
			(setq tempmirrorMode (getnth 3 tempRotations) )
		)
		(progn
			(setq tempgrainRadian (d2r tempRotations))
		)
	)

	(cleanVariables (list '__PANELDEPTH)) 
							  
	(set '__PANELDEPTH  (ifnull tempDepth (* ad_ET 10)))
	
	(if (and (member lmm-s-camProjectFile (list "10")) (not (checkStrInStr "!!SFACE" panelCode ))); Maestro specific
		(progn
			(setq panelEnt (lmm_drawPolyline (list (list 0.0 0.0 0.0) 0.0 (list 1.0 0.0 0.0) 0.0) "XLYP2" (setq tempReadColor (ifnull  (LMM_SFREAD lmm-s-panelcolor) "nil")) ))
			(lmm_createObjsLMMDATA panelEnt (strcat panelCode "_XLYP2") panelCode nil "_XLYP2" (list 1 0))
			(setq panelEnt (lmm_drawPolyline (list (list 0.0 0.0 0.0) 0.0 (list 1.0 0.0 0.0) 0.0) "XLYP3" (setq tempReadColor (ifnull  (LMM_SFREAD lmm-s-panelcolor) "nil")) ))
			(lmm_createObjsLMMDATA panelEnt (strcat panelCode "_XLYP3") panelCode nil "_XLYP3" (list 1 0))
			(setq panelEnt (lmm_drawPolyline (list (list 0.0 0.0 0.0) 0.0 (list 1.0 0.0 0.0) 0.0) "XLYP4" (setq tempReadColor (ifnull  (LMM_SFREAD lmm-s-panelcolor) "nil")) ))
			(lmm_createObjsLMMDATA panelEnt (strcat panelCode "_XLYP4") panelCode nil "_XLYP4" (list 1 0))
			(setq panelEnt (lmm_drawPolyline (list (list 0.0 0.0 0.0) 0.0 (list 1.0 0.0 0.0) 0.0) "XLYP5" (setq tempReadColor (ifnull  (LMM_SFREAD lmm-s-panelcolor) "nil")) ))
			(lmm_createObjsLMMDATA panelEnt (strcat panelCode "_XLYP5") panelCode nil "_XLYP5" (list 1 0))
		)
	)
	
	(setq panelEnt (lmm_drawPolyline tempPolylineData (setq tempReadLayer (ifnull  (LMM_SFREAD lmm-s-panellayer) "0")) (setq tempReadColor (ifnull  (LMM_SFREAD lmm-s-panelcolor) "nil"))  ))
	
	;SFACE : secondary face
	(if (checkStrInStr "!!SFACE" panelCode )	
		(lmm_createObjsLMMDATA panelEnt panelCode panelCode nil "PANEL_SFACE" (list 1 0))
		(lmm_createObjsLMMDATA panelEnt panelCode panelCode nil "PANEL" (list 1 1))
	)
	(setq lmm-panelIndex (ifnull lmm-panelIndex 0.0) )
	(setq lmm-panelIndex (+ 1 lmm-panelIndex))
	
	; change previously created panelEnt layer name as a PANELCONTOUR and draw a bouning box in panellayer if lmm-s-camProjectFile is Masterwood and Xilog panelEnt point count exceed 5,  
	(if (and (member lmm-s-camProjectFile (list "14" "9" "10")) (lmm_getIrregularPolyLines panelEnt)  )
		(progn
			(changeEntPro panelEnt 8 "PANELCONTOUR")
			(setq tempBBoxMW (getBBox panelEnt))
			(setq tempPolylineDataMW  (_RECT (nth 2 tempBBoxMW) (nth 3 tempBBoxMW) (nth 0 tempBBoxMW) ) )
			(setq panelEntMW (lmm_drawPolyline  tempPolylineDataMW  (setq tempReadLayer (ifnull  (LMM_SFREAD lmm-s-panellayer) "0")) (setq tempReadColor (ifnull  (LMM_SFREAD lmm-s-panelcolor) "nil")) ))
			(lmm_createObjsLMMDATA panelEntMW (strcat panelCode "_BBOX") panelCode nil "BBOX" (list 1 0))
		)
	)
	(set '__TOOLCOMPENSATION  "")
	(if (not (checkStrInStr "!!SFACE" panelCode ));sadece ana face de kertme olsun
		(if (not (equal (trim lmm-s-panelSizingLayer) ""))
			(progn
				(setq panelEnt (lmm_drawPolyline tempPolylineData (setq tempReadLayer (ifnull  (LMM_SFREAD lmm-s-panelSizingLayer) "0")) (setq tempReadColor (ifnull  (LMM_SFREAD lmm-s-panelSizingColor) "nil")) ))			
				(lmm_createObjsLMMDATA panelEnt (strcat panelCode "_PANEL_SIZING") panelCode nil "PANEL_SIZING" (list 1 0))
				(if (equal lmm-s-onlyIrregularSides "1")
					(progn			
						(foreach tempPolLines (lmm_getIrregularPolyLines panelEnt)	
							(set '__TOOLCOMPENSATION "INITIALVAL")
							(if (equal "CW" (GetIrregularPointlistClockwiseOrder tempPolLines  panelEnt) )
								(set '__TOOLCOMPENSATION "R")
								(set '__TOOLCOMPENSATION "L") 
							)
							(setq tempPolEnt (lmm_drawPolyline (lmm_convertPointListToPolylineData tempPolLines 0) (setq tempReadLayer (ifnull  (LMM_SFREAD lmm-s-panelSizingLayer) "0")) tempReadColor ))
							(lmm_createObjsLMMDATA tempPolEnt (strcat panelCode "_PANEL_SIZING") panelCode nil "PANEL_SIZING" (list 1 0))
						)					
						(deleteEntity panelEnt)
					)
				)
			)	
		)
	)
	
	(if (not (checkStrInStr "!!SFACE" panelCode ))
		(progn
			(setq
				uzunluk1 (/ (getnth 2 (getBBox (entlast))) 1000.0)
				uzunluk2 (/ (getnth 3 (getBBox (entlast))) 1000.0)
			)
			(setq 
				parcaM2 (* uzunluk1 uzunluk2)
				matTwoChar (substr tempMaterial 1 2)
			)
			(cond
				((member matTwoChar (list "CR" "MS"))
					(createItem "1 06 04 004" (list (* 0.16 parcaM2) "kg"))
					(createItem "1 07 02 001" (list (* 0.041 parcaM2) "kg"))
					(createItem "1 19 02 013" (list (* 0.082 parcaM2) "kg"))
					(createItem "1 07 02 001" (list (* 0.02 parcaM2) "kg"))
					(createItem "1 19 05 011" (list (* 0.05 parcaM2) "kg"))
					
					(cond
						((checkStrInStr "CRS01-BEYAZ" tempMaterial) (createItem "1 05 04 075" (list (* 0.1 parcaM2) "kg")))
						((checkStrInStr "CRS02-FILDISI" tempMaterial) (createItem "1 05 04 076" (list (* 0.1 parcaM2) "kg")))
						((checkStrInStr "CRS03-GRI" tempMaterial) (createItem "1 05 04 544" (list (* 0.1 parcaM2) "kg")))
						((checkStrInStr "CRS04-SARIBEJ" tempMaterial) (createItem "1 05 04 175" (list (* 0.1 parcaM2) "kg")))
						((checkStrInStr "MS01-BEYAZ" tempMaterial) (createItem "MISTO BEYAZ PARLAK LAKE" (list (* 0.1 parcaM2) "kg")))
						((checkStrInStr "MS02-FILDISI" tempMaterial) (createItem "MISTO FILDISI PARLAK LAKE" (list (* 0.1 parcaM2) "kg")))
						((checkStrInStr "MS03-CAPPUCINO" tempMaterial) (createItem "MISTO CAPUCINO PARLAK LAKE" (list (* 0.1 parcaM2) "kg")))
						((checkStrInStr "MS04-SIYAH" tempMaterial) (createItem "MISTO SIYAH PARLAK LAKE" (list (* 0.1 parcaM2) "kg")))
					)
				)
				((member matTwoChar (list "MN" "GS" "LC"))
					(createItem "1 04 01 095" (list (* 0.05 parcaM2) "kg"))
					(createItem "1 04 01 100" (list (* 0.0025 parcaM2) "kg"))
					
					(cond
						((checkStrInStr "MN01-BEYAZ" tempMaterial) (createItem "1 02 01 401" (list (* 2.5 parcaM2) "m2")))
						((checkStrInStr "MN02-FILDISI" tempMaterial) (createItem "1 02 01 449" (list (* 2.5 parcaM2) "m2")))
						((checkStrInStr "MN04-GRI" tempMaterial) (createItem "1 02 01 451" (list (* 2.5 parcaM2) "m2")))
						((checkStrInStr "MN05-KREM" tempMaterial) (createItem "1 02 01 450" (list (* 2.5 parcaM2) "m2")))
						((checkStrInStr "MN06-ITALYAN CEVIZ" tempMaterial) (createItem "1 02 01 026" (list (* 2.5 parcaM2) "m2")))
						((checkStrInStr "MN07-MILANO CEVIZ" tempMaterial) (createItem "1 02 01 021" (list (* 2.5 parcaM2) "m2")))
						((checkStrInStr "MN08-ACIK CEVIZ" tempMaterial) (createItem "1 02 01 305" (list (* 2.5 parcaM2) "m2")))
						((checkStrInStr "MN10-KOYU BAMBU" tempMaterial) (createItem "1 02 01 266" (list (* 2.5 parcaM2) "m2")))
						((checkStrInStr "GS01-BEYAZ" tempMaterial) (createItem "1 02 01 401" (list (* 2.5 parcaM2) "m2")))
						((checkStrInStr "GS02-FILDISI" tempMaterial) (createItem "1 02 01 449" (list (* 2.5 parcaM2) "m2")))
						((checkStrInStr "GS03-LATTE" tempMaterial) (createItem "1 02 01 429" (list (* 2.5 parcaM2) "m2")))
						((checkStrInStr "GS04-GRI" tempMaterial) (createItem "1 02 01 460" (list (* 2.5 parcaM2) "m2")))
						((checkStrInStr "GS05_ANTRASIT" tempMaterial) (createItem "1 02 01 428" (list (* 2.5 parcaM2) "m2")))
						((checkStrInStr "GS06DOGAL MESE" tempMaterial) (createItem "1 02 01 123" (list (* 2.5 parcaM2) "m2")))
						((checkStrInStr "GS07-GRI MESE" tempMaterial) (createItem "1 02 01 122" (list (* 2.5 parcaM2) "m2")))
						((checkStrInStr "GS08-ACIK VENGE" tempMaterial) (createItem "1 02 01 120" (list (* 2.5 parcaM2) "m2")))
						((checkStrInStr "GS09-ACIK CEVIZ" tempMaterial) (createItem "1 02 01 020" (list (* 2.5 parcaM2) "m2")))
						((checkStrInStr "GS10-KOYU CEVIZ" tempMaterial) (createItem "1 02 01 025" (list (* 2.5 parcaM2) "m2")))
						((checkStrInStr "GS11-ITALYAN MESE" tempMaterial) (createItem "1 02 01 124" (list (* 2.5 parcaM2) "m2")))
						((checkStrInStr "GS12-SUTLU KAHVE" tempMaterial) (createItem "1 02 01 125" (list (* 2.5 parcaM2) "m2")))
						((checkStrInStr "GS13-MOR" tempMaterial) (createItem "1 02 01 130" (list (* 2.5 parcaM2) "m2")))
						((checkStrInStr "GS14-TAS GRI" tempMaterial) (createItem "1 02 01 463" (list (* 2.5 parcaM2) "m2")))
						((checkStrInStr "LC01-BEYAZ" tempMaterial) (createItem "5 01 01 101" (list (* 2.5 parcaM2) "m2")))
						((checkStrInStr "LC02-FILDISI" tempMaterial) (createItem "P.FILDISI" (list (* 2.5 parcaM2) "m2")))
						((checkStrInStr "LC03-ANTRASIT" tempMaterial) (createItem "1 02 01 289" (list (* 2.5 parcaM2) "m2")))
						((checkStrInStr "LC04-LATTE" tempMaterial) (createItem "1 02 01 287" (list (* 2.5 parcaM2) "m2")))
						((checkStrInStr "LC05-CAPPUCINO" tempMaterial) (createItem "1 02 01 288" (list (* 2.5 parcaM2) "m2")))
					)
				)
				((member matTwoChar (list "LG"))
					(createItem "1 04 01 137" (list (* 0.3 parcaM2) "kg"))
					
					(cond
						((checkStrInStr "LG01-ABANOZ" tempMaterial) (createItem "1 20 01 003" (list (* 1.2 parcaM2) "kg")))
						((checkStrInStr "LG02-CEVIZ" tempMaterial) (createItem "1 20 01 091" (list (* 1.2 parcaM2) "kg")))
						((checkStrInStr "LG03-EXTRA TEAK" tempMaterial) (createItem "1 20 01 079" (list (* 1.2 parcaM2) "kg")))
						((checkStrInStr "LG04-SUTLU KAHVE" tempMaterial) (createItem "1 20 01 089" (list (* 1.2 parcaM2) "kg")))
						((checkStrInStr "LG05-TEAK" tempMaterial) (createItem "1 20 01 560" (list (* 1.2 parcaM2) "kg")))
					)
				)
				((member matTwoChar (list "BN"))
					(createItem "1 04 01 137" (list (* 0.3 parcaM2) "kg"))
					(createItem "1 18 01 070" (list (* 0.328 parcaM2) "kg"))
					(createItem "1 19 07 017" (list (* 0.066 parcaM2) "kg"))
					(createItem "1 07 03 035" (list (* 0.165 parcaM2) "lt"))
					(createItem "1 18 01 069" (list (* 0.42 parcaM2) "kg"))
					(createItem "1 19 07 017" (list (* 0.08 parcaM2) "kg"))
					(createItem "1 07 03 035" (list (* 0.22 parcaM2) "lt"))
					
					(cond
						((checkStrInStr "BN01-AFRIKA CEVIZ" tempMaterial) (createItem "1 20 01 100" (list (* 1.5 parcaM2) "m2")))
						((checkStrInStr "BN02-AMERIKAN CEVIZ" tempMaterial) (createItem "1 20 01 110" (list (* 1.5 parcaM2) "m2")))
						((checkStrInStr "BN02-AMERIKAN CEVIZ" tempMaterial) (createItem "1 20 01 110" (list (* 1.5 parcaM2) "m2")))
						((checkStrInStr "BN03-AMERIKAN KIRAZ" tempMaterial) (createItem "1 20 01 115" (list (* 1.5 parcaM2) "m2")))
						((checkStrInStr "BN04-BAMBU" tempMaterial) (createItem "1 20 01 120" (list (* 1.5 parcaM2) "m2")))
						((checkStrInStr "BN05-MAUN" tempMaterial) (createItem "1 20 01 150" (list (* 1.5 parcaM2) "m2")))
						((checkStrInStr "BN06-TEAK" tempMaterial) (createItem "1 20 01 130" (list (* 1.5 parcaM2) "m2")))
					)
				)
			)
		)
	)
)

(defun createItem (itemCode itemParams / )
	(_ITEMMAIN itemCode itemCode itemParams)
)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(prepareJSONCodeCSVIfNewer)
(loadSubstitudeTable)
(firm_startup)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(princ)